#include <string>
#include <omp.h>
#include "OFFModelLoader.h"


TriangleMesh* OFFModelLoader::loadFile(char* filename)
{
	printf("OFFModelLoader::loadFile()- Reading File \"%s\"...\n", filename);
	Tokenizer* token = new Tokenizer();
	bool success = token->Open(filename);
	char buf[50]; 

	if (success)
	{
		TriangleMesh* mesh = new TriangleMesh();

		success = token->GetToken(buf);
		if (success)
		{
			if (strcmp(buf, "OFF") != 0)
			{
				printf("ERROR: OFFModelLoader::loadFile()- Model file is not in OFF format.\n");
			}
			else
			{
				//Get # of vertex, faces
				mesh->vertexIndex = token->GetInt();
				mesh->faceIncex = token->GetInt();
				token->SkipLine();

				printf("OFFModelLoader::loadFile()- Loading Vertecies...\n");
				for (int i = 0; i < mesh->vertexIndex; i++)
				{
					float v1, v2, v3;
					v1 = token->GetFloat();
					v2 = token->GetFloat();
					v3 = token->GetFloat();

					Vertex* v = new Vertex(v1, v2, v3);
					mesh->vertexList[i] = v;
				}

				printf("OFFModelLoader::loadFile()- Loading Faces...\n");
				for (int i = 0; i < mesh->faceIncex; i++)
				{
					int f1, f2, f3;

					//dump data
					(void)token->GetInt();

					f1 = token->GetInt();
					f2 = token->GetInt();
					f3 = token->GetInt();

					if (f1 == 1 || f2 == 1 || f3 == 1)
					{
						int a = 0;
						a = 1;
					}

					Triangle* face = new Triangle(f1, f2, f3);
					face->color = Vector3((float)rand() / (RAND_MAX), (float)rand() / (RAND_MAX), (float)rand() / (RAND_MAX));
					mesh->face3List[i] = face;

					//add face to adj list of vertex;
					mesh->vertexFaceAdjList[f1].push_back(i);
					mesh->vertexFaceAdjList[f2].push_back(i);
					mesh->vertexFaceAdjList[f3].push_back(i);
				}

				return mesh;
			}
		}

		token->Close();
	}

	return NULL;
}