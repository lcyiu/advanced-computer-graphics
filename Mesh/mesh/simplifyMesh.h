#ifndef SIMPLIFYMESH_H
#define SIMPLIFYMESH_H

#define max3(a,b,c) (max((a), max((b),(c))))
#define min3(a,b,c) (min((a), min((b),(c))))

#include <algorithm>
#include <omp.h>
#include <queue>
#include <vector>
#include <list>
#include <ctime>
#include <stack>  
#include "TriangleMesh.h"
#include "Matrix4x4.h"
#include "Object.h"
#include "Settings.h"

struct newVertexPair
{
	int v1, v2;
	Vector3 pos;
	Matrix4x4 qMtx;
	bool isValid;
};

typedef pair<float, newVertexPair*> ErrorPair; //a pair with error at first and valid pair at second

class CompareError
{
public:
	bool operator()(ErrorPair e1, ErrorPair e2)
	{
		return e1.first > e2.first;
	}
};

class SimplifyMesh
{
public:
	static unordered_map<int, list<pair<int, newVertexPair*>>> validPairs;
	static priority_queue<ErrorPair, vector<ErrorPair>, CompareError> errorHeap;
	//static vector<ErrorPair> errorHeap;
	static stack<EdgeCollapseData *> collapsedStack;
	static stack<EdgeCollapseData *> splitedStack;
	static int totalValidPairs;
	static int totalEdge;
	static float avgDistance;

	static void Initiate(TriangleMesh* mesh);
	static void UpdateOldPairs(TriangleMesh* mesh, int v1, int newVertIndex, float err);
	static void UpdateAdjPairs(TriangleMesh* mesh, int v1, int oldV1, int oldV2, int newVertIndex, float err);
	static void Collapse(TriangleMesh* mesh);
	static void PreCollapse(TriangleMesh* mesh);
	static void Split(TriangleMesh* mesh, bool atSplitPos);
	static Matrix4x4 VertexQuadric(TriangleMesh* mesh, int index);
	static int GetTotalValidPairs();
	static void GetValidPairs(TriangleMesh* mesh);
	static void CalculateThreshold(TriangleMesh* mesh);
	static void UpdatePairPosition(TriangleMesh* mesh, int i, pair<int, newVertexPair*>& back);
	static void CalculatePairPositions(TriangleMesh* mesh);
	static float VertexError(Matrix4x4 q, float x, float y, float z);

	static EdgeCollapseData* getCollapse();
	static EdgeCollapseData* getSplit();
};

#endif