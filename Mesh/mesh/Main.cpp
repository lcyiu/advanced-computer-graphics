#include "display.h"

#define BUFSIZE 512

////////////////////////////////////////////////////////////////////////////////
Display *DISPLAY;

void glutFunctionSetup();
void gluiFunctionSetup();

int main(int argc, char **argv) {
	glutInit(&argc, argv);

	DISPLAY = new Display(argc, argv);

	glutFunctionSetup();
	glutMainLoop();

	return 0;
}

////////////////////////////////////////////////////////////////////////////////

// These are really HACKS to make glut call member functions instead of static functions
void display()									{ DISPLAY->Draw(); }
void idle()										{ DISPLAY->Update(); }
void resize(int x, int y)							{ DISPLAY->Resize(x, y); }
void keyboard(unsigned char key, int x, int y)		{ DISPLAY->Keyboard(key, x, y); }
void mousebutton(int btn, int state, int x, int y)	{ DISPLAY->MouseButton(btn, state, x, y); }
void mousemotion(int x, int y)					{ DISPLAY->MouseMotion(x, y); }

void glutFunctionSetup()
{
	// Callbacks
	glutDisplayFunc(display);
	glutIdleFunc(idle);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mousebutton);
	glutMotionFunc(mousemotion);
	glutPassiveMotionFunc(mousemotion);
	glutReshapeFunc(resize);

}