#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Object.h"
#include "Vertex.h"
#include "Matrix4x4.h"

////////////////////////////////////////////////////////////////////////////////

class Triangle : public Object {
public:
	Triangle(int ver1, int ver2, int ver3) : v1(ver1), v2(ver2), v3(ver3) {};

	void Update(){};
	void Reset(){};
	void Draw(){};
	void GetMinMax(Vector3& min, Vector3& max){};
	void CalcErrorQuadricMatrix(Vector3 v0)
	{
		float a = normal.x;
		float b = normal.y;
		float c = normal.z;
		float d = v0.Dot(-normal);

		Vector4 col1(a*a, a*b, a*c, a*d);
		Vector4 col2(a*b, b*b, b*c, b*d);
		Vector4 col3(a*c, b*c, c*c, c*d);
		Vector4 col4(a*d, b*d, c*d, d*d);
		quadricMtx.set(col1, col2, col3, col4);
	};

	Vector3 normal;
	Vector3 color;
	Matrix4x4 quadricMtx;
	int v1, v2, v3;

	int & operator[](int i) { return (&v1)[i]; }
	const int & operator[](int i) const { return (&v1)[i]; }
};

////////////////////////////////////////////////////////////////////////////////

#endif
