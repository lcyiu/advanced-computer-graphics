#include "TriangleMesh.h"
#include "simplifyMesh.h"
#include "Settings.h"

#include <omp.h>


void TriangleMesh::Draw()
{
	GLfloat mat_a[] = { 0.1f, 0.1f, 0.1f, 1.0f };
	GLfloat mat_s[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat low_sh[] = { 100.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_a);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_s);
	glMaterialfv(GL_FRONT, GL_SHININESS, low_sh);


	for (auto& iterator : face3List)
	{
		Triangle *face = iterator.second;
		Vertex* v1 = vertexList[face->v1];
		Vertex* v2 = vertexList[face->v2];
		Vertex* v3 = vertexList[face->v3];

		if (Settings::_displayWireFrame)
		{
			GLfloat mat_d2[] = { 0.8f, 0.8f, 0.8f, 0.8f };
			glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_d2);

			glBegin(GL_LINE_LOOP);
			glNormal3f(v1->normal.x, v1->normal.y, v1->normal.z);
			glVertex3f(v1->position.x, v1->position.y, v1->position.z);
			glNormal3f(v2->normal.x, v2->normal.y, v2->normal.z);
			glVertex3f(v2->position.x, v2->position.y, v2->position.z);
			glNormal3f(v3->normal.x, v3->normal.y, v3->normal.z);
			glVertex3f(v3->position.x, v3->position.y, v3->position.z);
			glEnd();
		}
		else
		{
			GLfloat mat_d[4];

			if (Settings::_greyColor)
			{
				mat_d[0] = mat_d[1] = mat_d[2] = 0.8f;
				mat_d[4] = 1;
			}
			else
			{
				mat_d[0] = face->color.x;
				mat_d[1] = face->color.y;
				mat_d[2] = face->color.z;
				mat_d[3] = 1;
			}

			glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_d);

			glBegin(GL_TRIANGLES);
			glNormal3f(v1->normal.x, v1->normal.y, v1->normal.z);
			glVertex3f(v1->position.x, v1->position.y, v1->position.z);
			glNormal3f(v2->normal.x, v2->normal.y, v2->normal.z);
			glVertex3f(v2->position.x, v2->position.y, v2->position.z);
			glNormal3f(v3->normal.x, v3->normal.y, v3->normal.z);
			glVertex3f(v3->position.x, v3->position.y, v3->position.z);
			glEnd();

		}
	}

	if (Settings::_displayNormals)
	{
		glBegin(GL_LINES);
		for (auto& iterator : face3List)
		{
			Triangle *face = iterator.second;
			Vertex* v1 = vertexList[face->v1];
			Vertex* v2 = vertexList[face->v2];
			Vertex* v3 = vertexList[face->v3];
			float scale = SimplifyMesh::avgDistance;

			GLfloat mat_d[] = { face->color.x, face->color.y, face->color.z, 1.0f };
			glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_d);

			glVertex3f(v1->position.x, v1->position.y, v1->position.z);
			glVertex3f(v1->position.x + v1->normal.x * scale, v1->position.y + v1->normal.y * scale, v1->position.z + v1->normal.z * scale);

			glVertex3f(v2->position.x, v2->position.y, v2->position.z);
			glVertex3f(v2->position.x + v2->normal.x * scale, v2->position.y + v2->normal.y * scale, v2->position.z + v2->normal.z * scale);

			glVertex3f(v3->position.x, v3->position.y, v3->position.z);
			glVertex3f(v3->position.x + v3->normal.x * scale, v3->position.y + v3->normal.y * scale, v3->position.z + v3->normal.z * scale);

		}
		glEnd();
	}
}

void TriangleMesh::PreCalc()
{
	printf("TriangleMesh::PreCalc()- Calculating All Faces' Normal...\n");
	//#pragma omp parallel for 
	for (auto& iterator : face3List)
	{
		Triangle *face = iterator.second;
		CalcTriangleNormal(face);
	}

	//#pragma omp parallel for 
	printf("TriangleMesh::PreCalc()- Calculating Per Vertex Normal...\n");
	for (auto& it : vertexList)
	{
		CalcVertexNormal(it);
	}

	printf("TriangleMesh::PreCalc()- Calulating Matrix For Centering Model...\n");
	Vector3 min, max, center;
	GetMinMax(min, max);
	center = (min + max) / 2.0f;
	Translate(-center.x, -center.y, -center.z);
}

void TriangleMesh::CalcTriangleNormal(Triangle* face)
{
	Vertex* v1 = vertexList[face->v1];
	Vertex* v2 = vertexList[face->v2];
	Vertex* v3 = vertexList[face->v3];

	Vector3 v1v2 = v2->position - v1->position;
	Vector3 v1v3 = v3->position - v1->position;

	Vector3 normal;
	normal.Cross(v1v2, v1v3);
	normal.Normalize();
	face->normal = normal;
}

void TriangleMesh::CalcVertexNormal(pair<const int, Vertex*>& it)
{
	FaceAdjList adjList = vertexFaceAdjList[it.first];
	Vertex* v = it.second;
	Vector3 avgNormal;

	FaceAdjList::const_iterator iterator;
	for (iterator = adjList.begin(); iterator != adjList.end(); iterator++)
	{
		avgNormal += face3List[*iterator]->normal;
	}

	avgNormal *= 1.0f / adjList.size();
	avgNormal.Normalize();

	v->normal = avgNormal;
};

void TriangleMesh::Split(EdgeCollapseData* ecd, bool atSplitPos)
{
	int splitVertex = ecd->newV.first;

	list<pair<int, Triangle*>>::const_iterator iterator;
	for (iterator = ecd->removedFace.begin(); iterator != ecd->removedFace.end(); iterator++)
	{
		Triangle* t = (*iterator).second;
		face3List[(*iterator).first] = t;

		vertexFaceAdjList[t->v1].push_back((*iterator).first);
		vertexFaceAdjList[t->v2].push_back((*iterator).first);
		vertexFaceAdjList[t->v3].push_back((*iterator).first);
	}

	for (int i = 0; i < 2; i++)
	{
		int vIndex = ecd->v[i].first;
		if (atSplitPos)
		{
			vertexList[vIndex]->position = ecd->newV.second;
		}
		else
		{
			vertexList[vIndex]->position = ecd->v[i].second;
		}

		list<pair<int, int>>::const_iterator iterator;
		for (iterator = ecd->vData[i].begin(); iterator != ecd->vData[i].end(); iterator++)
		{
			vertexFaceAdjList[vIndex].push_back((*iterator).first);
			(*face3List[(*iterator).first])[(*iterator).second] = vIndex;
		}
	}

	//list<int>::const_iterator normalIt;
	//for (normalIt = ecd->affectedFace.begin(); normalIt != ecd->affectedFace.end(); normalIt++)
	//{
	//	CalcTriangleNormal(face3List[*normalIt]);
	//}

	//for (iterator = ecd->removedFace.begin(); iterator != ecd->removedFace.end(); iterator++)
	//{
	//	CalcTriangleNormal((*iterator).second);
	//}

	//for (normalIt = ecd->affectedVertex.begin(); normalIt != ecd->affectedVertex.end(); normalIt++)
	//{
	//	Vertex* v = vertexList[*normalIt];
	//	CalcVertexNormal(pair<const int, Vertex*>(*normalIt, v));
	//}

	//CalcVertexNormal(pair<const int, Vertex*>(ecd->v[0].first, vertexList[ecd->v[0].first]));
	//CalcVertexNormal(pair<const int, Vertex*>(ecd->v[1].first, vertexList[ecd->v[1].first]));

	vertexFaceAdjList[splitVertex].clear();
}

void TriangleMesh::Collapse(EdgeCollapseData* ecd)
{
	int newVertIndex = ecd->newV.first;
	vertexList[newVertIndex]->position = ecd->newV.second;
	vertexList[newVertIndex]->normal = ecd->newVN;

	list<pair<int, int>>::const_iterator vData_It;
	for (int i = 0; i < 2; i++)
	{
		for (vData_It = ecd->vData[i].begin(); vData_It != ecd->vData[i].end(); vData_It++)
		{
			(*face3List[(*vData_It).first])[(*vData_It).second] = newVertIndex;
			vertexFaceAdjList[newVertIndex].push_back((*vData_It).first);
		}

		vertexFaceAdjList.erase(ecd->v[i].first);
	}

	vertexFaceAdjList[newVertIndex].sort();
	vertexFaceAdjList[newVertIndex].unique();

	list<pair<int, Triangle*>>::const_iterator face_It;
	for (face_It = ecd->removedFace.begin(); face_It != ecd->removedFace.end(); face_It++)
	{
		Triangle* t = (*face_It).second;
		RemoveTriangleFromList((*face_It).first, t);
	}

	//list<int>::const_iterator normalIt;
	//for (normalIt = ecd->affectedFace.begin(); normalIt != ecd->affectedFace.end(); normalIt++)
	//{
	//	CalcTriangleNormal(face3List[*normalIt]);
	//}

	//for (normalIt = ecd->affectedVertex.begin(); normalIt != ecd->affectedVertex.end(); normalIt++)
	//{
	//	Vertex* v = vertexList[*normalIt];
	//	CalcVertexNormal(pair<const int, Vertex*>(*normalIt, v));
	//}
}

EdgeCollapseData* TriangleMesh::Collapse(int v1, int v2, Vector3& dstV)
{
	//printf("TriangleMesh::Collapse()\n\t-Collapsing vertex %d, %d to new vertex %d(%.2f, %.2f, %.2f)\n", v1, v2, vertexIndex, dstV.x, dstV.y, dstV.z);
	Vertex* vert1 = vertexList[v1];
	Vertex* vert2 = vertexList[v2];
	unordered_map<int, int> affectedVertex;
	Vertex* newV = new Vertex(dstV);
	vertexList[vertexIndex] = newV;

	EdgeCollapseData* ecd = new EdgeCollapseData();
	ecd->v[0] = pair<int, Vector3>(v1, vert1->position);
	ecd->v[1] = pair<int, Vector3>(v2, vert2->position);
	ecd->newV = pair<int, Vector3>(vertexIndex, dstV);
	//ecd->newVN = vert1->normal;
	ecd->newVN.Normalize();

	//change adj face to use new v instead of v1, v2
	ChangeFaceVertices(v1, ecd, 0);
	ChangeFaceVertices(v2, ecd, 1);

	//check mirrored polygons and concide face
	vertexFaceAdjList[vertexIndex].sort();
	vertexFaceAdjList[vertexIndex].unique();
	DegenConcideFace(vertexIndex, ecd);
	if (!Settings::_allowFins)
	{
		DegenMirrorPolygon(vertexIndex);
	}

	//update normal
	FaceAdjList dstAdj = vertexFaceAdjList[vertexIndex];
	FaceAdjList::const_iterator iterator;
	for (iterator = dstAdj.begin(); iterator != dstAdj.end(); iterator++)
	{
		Triangle* t = face3List[*iterator];
		CalcTriangleNormal(t);

		affectedVertex[t->v1] = 1;
		affectedVertex[t->v2] = 1;
		affectedVertex[t->v3] = 1;

		//ecd->affectedFace.push_back(*iterator);
	}

	for (auto& it : affectedVertex)
	{
		int first = it.first;
		Vertex* v = vertexList[first];
		//CalcVertexNormal(pair<const int, Vertex*>(first, v));

		//ecd->affectedVertex.push_back(first);
	}

	CalcVertexNormal(pair<const int, Vertex*>(vertexIndex, vertexList[vertexIndex]));
	ecd->newVN = vertexList[vertexIndex]->normal;

	//return vertexIndex++;
	vertexIndex++;
	return ecd;
}

void TriangleMesh::ChangeFaceVertices(int v, EdgeCollapseData* ecd, int vData)
{
	//printf("\tTriangleMesh::ChangeFaceVertices()\n");
	pair<int, int> faceData;
	FaceAdjList::const_iterator iterator;
	for (iterator = vertexFaceAdjList[v].begin(); iterator != vertexFaceAdjList[v].end(); iterator++)
	{
		//need change
		Triangle* t = face3List[*iterator];
		faceData.first = *iterator;

		if (t->v1 == v)
		{
			faceData.second = 0;
			t->v1 = vertexIndex;
		}
		else if (t->v2 == v)
		{
			faceData.second = 1;
			t->v2 = vertexIndex;
		}
		else
		{
			faceData.second = 2;
			t->v3 = vertexIndex;
		}

		ecd->vData[vData].push_back(faceData);
		vertexFaceAdjList[vertexIndex].push_back(*iterator);
	}

	//printf("\t\t-Removing vertex %d from vertexList, faceAdjList\n", v);
	vertexFaceAdjList.erase(v);
	//vertexList.erase(v);
}

void TriangleMesh::DegenConcideFace(int v, EdgeCollapseData* ecd)
{
	//printf("\tTriangleMesh::DegenConcideFace()\n");
	FaceAdjList vAdj = vertexFaceAdjList[v];
	FaceAdjList::const_iterator iterator;
	for (iterator = vAdj.begin(); iterator != vAdj.end(); iterator++)
	{
		Triangle* t = face3List[*iterator];
		if (CheckConcideVertices(t->v1, t->v2, t->v3))
		{
			//TODO Degen face
			//printf("\t\t-Face %d is degenarated!!\n", *iterator);
			ecd->removedFace.push_back(pair<int, Triangle*>(*iterator, t));
			RemoveTriangleFromList(*iterator, t);
		}
	}
}

void TriangleMesh::DegenMirrorPolygon(int faceAdjIndex)
{
	//need to fix
	FaceAdjList::const_iterator iterator;
	for (iterator = vertexFaceAdjList[faceAdjIndex].begin(); iterator != vertexFaceAdjList[faceAdjIndex].end(); iterator++)
	{
		Triangle* t = face3List[*iterator];
		Vector3 v1Pos = vertexList[t->v1]->position;
		Vector3 v2Pos = vertexList[t->v2]->position;
		Vector3 v3Pos = vertexList[t->v3]->position;

		FaceAdjList::const_iterator iterator2;
		for (iterator2 = (next(iterator)); iterator2 != vertexFaceAdjList[faceAdjIndex].end(); iterator2++)
		{
			Triangle* t2 = face3List[*iterator2];
			Vector3 v1Pos2 = vertexList[t2->v1]->position;
			Vector3 v2Pos2 = vertexList[t2->v2]->position;
			Vector3 v3Pos2 = vertexList[t2->v3]->position;

			if (CheckSamePolygon(v1Pos, v2Pos, v3Pos, v1Pos2, v2Pos2, v3Pos2))
			{ 
				//printf("\tTriangleMesh::DegenMirrorPolygon()\n\t\t-Face %d, %d are mirror polygons. Removing...\n", *iterator, *iterator2);
				int tIndex = *iterator;
				int tIndex2 = *iterator2;

				if (next(iterator) == iterator2)
				{
					iterator = next(iterator2);
				}
				else
				{
					iterator++;
				}

				iterator2 = iterator;

				RemoveTriangleFromList(tIndex, t);
				RemoveTriangleFromList(tIndex2, t2);
			}
		}
	}
}

void TriangleMesh::RemoveTriangleFromList(int tIndex, Triangle* t)
{
	//printf("\t\tTriangleMesh::RemoveTriangleFromList()\n\t\t\t-Removing triangle %d from faceAdjList of %d, %d, %d\n", tIndex, t->v1, t->v2, t->v3);
	vertexFaceAdjList[t->v1].remove(tIndex);
	vertexFaceAdjList[t->v2].remove(tIndex);
	vertexFaceAdjList[t->v3].remove(tIndex);
	//printf("\t\t\t-Removing triangle %d from faceList\n", tIndex);
	face3List.erase(tIndex);

	////remove vertices if needed
	//if (vertexFaceAdjList[t->v1].size() < 1)
	//{
	//	printf("\t\t\t-V%d no adjface. Removing from vertexList, faceAdjList\n", t->v1);
	//	vertexList.erase(t->v1);
	//	vertexFaceAdjList.erase(t->v1);
	//}

	//if (vertexFaceAdjList[t->v2].size() < 1)
	//{
	//	printf("\t\t\t-V%d no adjface. Removing from vertexList, faceAdjList\n", t->v2);
	//	vertexList.erase(t->v2);
	//	vertexFaceAdjList.erase(t->v2);
	//}

	//if (vertexFaceAdjList[t->v3].size() < 1)
	//{
	//	printf("\t\t\t-V%d no adjface. Removing from vertexList, faceAdjList\n", t->v3);
	//	vertexList.erase(t->v3);
	//	vertexFaceAdjList.erase(t->v3);
	//}
}

bool TriangleMesh::CheckSamePolygon(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 v5, Vector3 v6)
{
	bool comp1 = (v1 == v4) && ((v2 == v5 && v3 == v6) || (v3 == v5 && v2 == v6));
	bool comp2 = (v2 == v4) && ((v1 == v5 && v3 == v6) || (v3 == v5 && v1 == v6));
	bool comp3 = (v3 == v4) && ((v1 == v5 && v2 == v6) || (v2 == v5 && v1 == v6));

	return comp1 || comp2 || comp3;
}

bool TriangleMesh::CheckConcideVertices(int v1, int v2, int v3)
{
	return (v1 == v2 || v1 == v3 || v2 == v3);
}

void TriangleMesh::GetMinMax(Vector3& min, Vector3& max)
{
	Vector3 minR((float)MAXINT, (float)MAXINT, (float)MAXINT);
	Vector3 maxR((float)MININT, (float)MININT, (float)MININT);

	for (auto& iterator : vertexList)
	{
		Vector3 v = iterator.second->position;

		if (minR.x > v.x)
		{
			minR.x = v.x;
		}

		if (maxR.x < v.x)
		{
			maxR.x = v.x;
		}

		if (minR.y > v.y)
		{
			minR.y = v.y;
		}

		if (maxR.y < v.y)
		{
			maxR.y = v.y;
		}

		if (minR.z > v.z)
		{
			minR.z = v.z;
		}

		if (maxR.z < v.z)
		{
			maxR.z = v.z;
		}
	}

	min = minR;
	max = maxR;
}