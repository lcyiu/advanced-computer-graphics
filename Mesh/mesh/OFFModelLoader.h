#ifndef OFFMODELLOADER_H
#define OFFMODELLOADER_H

#include <string>
#include "TriangleMesh.h"
#include "token.h"

class OFFModelLoader
{
public:
	static TriangleMesh* loadFile(char* filename);
};

#endif