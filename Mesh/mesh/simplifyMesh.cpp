#include "simplifyMesh.h"

//#define METHOD1

unordered_map<int, list<pair<int, newVertexPair*>>> SimplifyMesh::validPairs;
//priority_queue<ErrorPair, vector<ErrorPair>, CompareError> SimplifyMesh::errorHeap = priority_queue<ErrorPair, vector<ErrorPair>, CompareError>();
//vector<ErrorPair> SimplifyMesh::errorHeap;
priority_queue<ErrorPair, vector<ErrorPair>, CompareError> SimplifyMesh::errorHeap;
stack<EdgeCollapseData *> SimplifyMesh::collapsedStack;
stack<EdgeCollapseData *> SimplifyMesh::splitedStack;
int SimplifyMesh::totalValidPairs = 0;
int SimplifyMesh::totalEdge = 0;
float SimplifyMesh::avgDistance = 0.0f;

void SimplifyMesh::Initiate(TriangleMesh* mesh)
{
	clock_t beginT = clock();
	printf("\n                   ****** INITIATE MESH SIMPLIFICATION ******                    \n\n");
	printf("SimpplifyMesh::Initiate()- Calulating Quadric Error Metrics...\n");
#pragma omp parallel for 
	for (int i = 0; i < mesh->face3List.size(); i++)
	{
		Triangle *face = mesh->face3List[i];
		face->CalcErrorQuadricMatrix(mesh->vertexList[face->v1]->position);
	}

	int j = 0;
#pragma omp parallel for 
	for (int i = 0; i < mesh->vertexFaceAdjList.size(); i++)
	{
		Vertex* vertex = mesh->vertexList[i];
		vertex->quadricMtx = VertexQuadric(mesh, i);
	}

	CalculateThreshold(mesh);
	printf("SimpplifyMesh::Initiate()- Getting All Valid Pair With Threshold t = %.2f...\n", Settings::_t);
	GetValidPairs(mesh);
	printf("SimpplifyMesh::Initiate()- Total Valid Pairs: %d Total Edge: %d\n", totalValidPairs, totalEdge);
	printf("SimpplifyMesh::Initiate()- Calculating Optimal Collapse Position...\n");
	CalculatePairPositions(mesh);
	printf("SimpplifyMesh::Initiate()- Initiation Time: %.2fs\n", (float)(clock() - beginT) / CLOCKS_PER_SEC);
	beginT = clock();
	int minFace = 0;//(int)(mesh->face3List.size() * 0.001f);
	int oriFace = (int)mesh->face3List.size();
	printf("SimpplifyMesh::Initiate()- Total face: %d Min Face: %d\n", oriFace, minFace);
	while (mesh->face3List.size() > minFace)
	{
		SimplifyMesh::PreCollapse(mesh);

		//printf("%d\n", mesh->face3List.size());
		if (mesh->face3List.size() % 1000 <= 1)
		{
			printf("SimpplifyMesh::Initiate()- Collapsing... %.1f%% %.1fs\r",
				(oriFace - mesh->face3List.size()) / (float)oriFace * 100.0f, (float)(clock() - beginT) / CLOCKS_PER_SEC, errorHeap.size(), GetTotalValidPairs());
		}
	}

	printf("\nSimpplifyMesh::Initiate()- Splitting Back Model...\n\n\n");
	while (!splitedStack.empty())
	{
		Split(mesh, false);
	}

};

void SimplifyMesh::UpdateOldPairs(TriangleMesh* mesh, int v1, int newVertIndex, float err)
{
	list<pair<int, newVertexPair*>>::iterator iterator;
	pair<int, newVertexPair*> updatedPair(newVertIndex, NULL);
	for (iterator = validPairs[v1].begin(); iterator != validPairs[v1].end(); iterator++)
	{
		newVertexPair* oldNVP = (*iterator).second;
		int updatedVert = (oldNVP->v1 == v1) ? oldNVP->v2 : oldNVP->v1;
		if (mesh->vertexFaceAdjList[updatedVert].size() > 0)
		{
			list<pair<int, newVertexPair*>>::iterator it;
			it = find_if(validPairs[updatedVert].begin(), validPairs[updatedVert].end(), [newVertIndex](pair<int, newVertexPair*> const &b) {
				return b.first == newVertIndex;
			});
			if (it == validPairs[updatedVert].end())
			{
				validPairs[updatedVert].push_back(updatedPair);
				UpdatePairPosition(mesh, updatedVert, validPairs[updatedVert].back());
			}
		}

#ifdef METHOD1

		vector<ErrorPair>::iterator it = std::lower_bound(errorHeap.begin(), errorHeap.end(), err, [](ErrorPair ep, double er)
		{
			return ep.first < er;
		});
		for (std::vector<ErrorPair>::iterator iter = it; iter != errorHeap.end(); iter++)
		{
			if ((*iter).second->v1 == oldNVP->v1 && (*iter).second->v2 == oldNVP->v2)
			{

				errorHeap.erase(iter);
				break;
			}
		}

		delete (*iterator).second;
		(*iterator).second = 0;

#else

		(*iterator).second->isValid = false;

#endif
	}

	validPairs.erase(v1);
}

void SimplifyMesh::UpdateAdjPairs(TriangleMesh* mesh, int v1, int oldV1, int oldV2, int newVertIndex, float err)
{
	int validPairsSize = (int)validPairs[v1].size();
	list<pair<int, newVertexPair*>>::iterator iterator = validPairs[v1].begin();;
	pair<int, newVertexPair*> updatedPair(newVertIndex, NULL);
	for (int i = 0; i < validPairsSize; i++)
	{
		if ((*iterator).first == oldV1 || (*iterator).first == oldV2)
		{

			list<pair<int, newVertexPair*>>::iterator it;
			if (mesh->vertexFaceAdjList[v1].size() > 0)
			{
				it = find_if(validPairs[v1].begin(), validPairs[v1].end(), [newVertIndex](pair<int, newVertexPair*> const &b) {
					return b.first == newVertIndex;
				});
				if (it == validPairs[v1].end())
				{
					validPairs[v1].push_back(updatedPair);
					UpdatePairPosition(mesh, v1, validPairs[v1].back());
				}
			}

#ifdef METHOD1
			vector<ErrorPair>::iterator it2 = std::lower_bound(errorHeap.begin(), errorHeap.end(), err, [](ErrorPair ep, double er)
			{
				return ep.first < er;
			});
			for (std::vector<ErrorPair>::iterator iter = it2; iter != errorHeap.end(); iter++)
			{
				if ((*iter).second->v1 == (*iterator).second->v1 && (*iter).second->v2 == (*iterator).second->v2)
				{
					errorHeap.erase(iter);
					break;
				}
			}

			delete (*iterator).second;
			(*iterator).second = 0;

#else

			(*iterator).second->isValid = false;

#endif

			pair<int, newVertexPair*> a = *iterator;
			iterator++;
			validPairs[v1].remove(a);
		}
		else
		{
			iterator++;
		}
	}
}

void SimplifyMesh::Split(TriangleMesh* mesh, bool atSplitPos)
{
	if (!splitedStack.empty())
	{
		EdgeCollapseData* ecd = splitedStack.top();
		mesh->Split(ecd, atSplitPos);
		
		collapsedStack.push(ecd);
		splitedStack.pop();
	}
}

void SimplifyMesh::Collapse(TriangleMesh* mesh)
{
	if (!collapsedStack.empty())
	{
		EdgeCollapseData* ecd = collapsedStack.top();
		mesh->Collapse(ecd);
		splitedStack.push(ecd);
		collapsedStack.pop();
	}
}

EdgeCollapseData* SimplifyMesh::getCollapse()
{
	if (!collapsedStack.empty())
	{
		return collapsedStack.top();
	}

	return NULL;
}

EdgeCollapseData* SimplifyMesh::getSplit()
{
	if (!splitedStack.empty())
	{
		return splitedStack.top();
	}

	return NULL;
}

int SimplifyMesh::GetTotalValidPairs()
{
	int total = 0;
	for (int i = 0; i < validPairs.size(); i++)
	{
		total += (int)validPairs[i].size();
	}

	return total;
}

void SimplifyMesh::PreCollapse(TriangleMesh* mesh)
{
	if (!errorHeap.empty())
	{
		//float err = errorHeap.back().first;
		//newVertexPair* nvp = errorHeap.back().second;
		//errorHeap.pop_back();

		float err = errorHeap.top().first;
		newVertexPair* nvp = errorHeap.top().second;
		errorHeap.pop();

		while (!nvp->isValid)
		{
			delete nvp;
			err = errorHeap.top().first;
			nvp = nvp = errorHeap.top().second;
			errorHeap.pop();

			//err = errorHeap.back().first;
			//nvp = nvp = errorHeap.back().second;
			//errorHeap.pop_back();
		}

		if (nvp->isValid)
		{
			EdgeCollapseData* ecd = mesh->Collapse(nvp->v1, nvp->v2, nvp->pos);
			splitedStack.push(ecd);

			int newVertIndex = ecd->newV.first;
			Vector3 newPos = nvp->pos;
			Matrix4x4 newQ = nvp->qMtx;
			int v1 = nvp->v1;
			int v2 = nvp->v2;
			mesh->vertexList[newVertIndex]->quadricMtx = newQ;

			//update pair v1, v2 to newVertIndex
			UpdateOldPairs(mesh, v1, newVertIndex, err);
			UpdateOldPairs(mesh, v2, newVertIndex, err);

			for (auto iterator = mesh->vertexFaceAdjList[newVertIndex].begin(); iterator != mesh->vertexFaceAdjList[newVertIndex].end(); iterator++)
			{
				Triangle* t = mesh->face3List[*iterator];
				int tV1 = 0;
				int tV2 = 0;
				if (t->v1 == newVertIndex)
				{
					tV1 = t->v2;
					tV2 = t->v3;
				}
				else if (t->v2 == newVertIndex)
				{
					tV1 = t->v1;
					tV2 = t->v3;
				}
				else
				{
					tV1 = t->v1;
					tV2 = t->v2;
				}

				UpdateAdjPairs(mesh, tV1, v1, v2, newVertIndex, err);
				UpdateAdjPairs(mesh, tV2, v1, v2, newVertIndex, err);
			}
		}
	}
}

Matrix4x4 SimplifyMesh::VertexQuadric(TriangleMesh* mesh, int index)
{
	Matrix4x4 qMtx(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	Matrix4x4 kMtx;
	FaceAdjList adjList = mesh->vertexFaceAdjList[index];

	FaceAdjList::const_iterator iterator;
	for (iterator = adjList.begin(); iterator != adjList.end(); iterator++)
	{
		qMtx += mesh->face3List[*iterator]->quadricMtx;
	}

	return qMtx;
}

void SimplifyMesh::GetValidPairs(TriangleMesh* mesh)
{
	//first check all vertices with threshold
	if (Settings::_t > 0.0f)
	{
		for (int i = 0; i < mesh->vertexList.size(); i++)
		{
			for (int j = i + 1; j < mesh->vertexList.size(); j++)
			{
				float dis = (mesh->vertexList[i]->position - mesh->vertexList[j]->position).Mag();
				if (dis < Settings::_t)
				{
					if (i < j)
					{
						pair<int, newVertexPair*> p(j, NULL);
						validPairs[i].push_back(p);
						totalValidPairs++;
					}
					else
					{
						pair<int, newVertexPair*> p(i, NULL);
						validPairs[j].push_back(p);
						totalValidPairs++;
					}
				}
			}
		}
	}


	//then all edges
	for (int i = 0; i < mesh->face3List.size(); i++)
	{
		Triangle *face = mesh->face3List[i];
		int smallest = min3(face->v1, face->v2, face->v3);
		int largest = max3(face->v1, face->v2, face->v3);
		int mid = 0;

		if (face->v1 == smallest || face->v1 == largest)
		{
			if (face->v2 == smallest || face->v2 == largest)
			{
				mid = face->v3;
			}
			else
			{
				mid = face->v2;
			}
		}
		else
		{
			mid = face->v1;
		}

		list<pair<int, newVertexPair*>>::iterator it;
		pair<int, newVertexPair*> p(mid, NULL);
		it = find_if(validPairs[smallest].begin(), validPairs[smallest].end(), [mid](pair<int, newVertexPair*> const &b) {
			return b.first == mid;
		});
		if (it == validPairs[smallest].end())
		{
			validPairs[smallest].push_back(p);
			totalValidPairs++;
		}

		p.first = largest;
		it = find_if(validPairs[smallest].begin(), validPairs[smallest].end(), [largest](pair<int, newVertexPair*> const &b) {
			return b.first == largest;
		});
		if (it == validPairs[smallest].end())
		{
			validPairs[smallest].push_back(p);
			totalValidPairs++;
		}

		it = find_if(validPairs[mid].begin(), validPairs[mid].end(), [largest](pair<int, newVertexPair*> const &b) {
			return b.first == largest;
		});
		if (it == validPairs[mid].end())
		{
			validPairs[mid].push_back(p);
			totalValidPairs++;
		}
	}
}

void SimplifyMesh::CalculateThreshold(TriangleMesh* mesh)
{

	//first check all vertices with threshold
	for (int i = 0; i < mesh->face3List.size(); i++)
	{
		Triangle *face = mesh->face3List[i];
		Vertex* v1 = mesh->vertexList[face->v1];
		Vertex* v2 = mesh->vertexList[face->v2];
		Vertex* v3 = mesh->vertexList[face->v3];
		avgDistance += (v2->position - v1->position).Mag();
		avgDistance += (v3->position - v1->position).Mag();
		avgDistance += (v2->position - v3->position).Mag();
		totalEdge += 3;
	}

	avgDistance /= (float)totalEdge;
	//Settings::_t = distance / (float)totalEdge;
}

void SimplifyMesh::UpdatePairPosition(TriangleMesh* mesh, int i, pair<int, newVertexPair*>& back)
{
	Matrix4x4 q1 = mesh->vertexList[i]->quadricMtx;
	Matrix4x4 q2 = mesh->vertexList[back.first]->quadricMtx;
	Matrix4x4 q = q1 + q2;
	Matrix4x4 qn = q;
	qn.m41 = 0;
	qn.m42 = 0;
	qn.m43 = 0;
	qn.m44 = 1;

	Vector3 res;
	float det = qn.det();
	float vertErr = 0.0f;

	newVertexPair* nvp = new newVertexPair();
	nvp->v1 = i;
	nvp->v2 = back.first;
	nvp->qMtx = q;
	nvp->isValid = true;

	if (det != 0)
	{
		qn.invert();
		res.x = qn.m14;
		res.y = qn.m24;
		res.z = qn.m34;

		vertErr = VertexError(q, res.x, res.y, res.z);
		nvp->pos = res;
	}
	else
	{
		Vector3 p1 = mesh->vertexList[i]->position;
		Vector3 p2 = mesh->vertexList[back.first]->position;
		Vector3 p3 = (p1 + p2) / 2.0f;
		float p1Err = VertexError(q, p1.x, p1.y, p1.z);
		float p2Err = VertexError(q, p2.x, p2.y, p2.z);
		float p3Err = VertexError(q, p3.x, p3.y, p3.z);
		vertErr = fmin(fmin(p1Err, p2Err), p3Err);

		res = (vertErr == p1Err) ? p1 : ((vertErr == p2Err) ? p2 : p3);
		nvp->pos = res;
	}

	back.second = nvp;
	ErrorPair ep(vertErr, nvp);
	//vector<ErrorPair>::iterator it = std::lower_bound(errorHeap.begin(), errorHeap.end(), ep, CompareError());
	//errorHeap.insert(it, ep);
	errorHeap.push(ep);
}

void SimplifyMesh::CalculatePairPositions(TriangleMesh* mesh)
{
	for (int i = 0; i < validPairs.size(); i++)
	{
		list<pair<int, newVertexPair*>>::iterator iterator;
		for (iterator = validPairs[i].begin(); iterator != validPairs[i].end(); iterator++)
		{
			UpdatePairPosition(mesh, i, (*iterator));
		}
	}
}

float SimplifyMesh::VertexError(Matrix4x4 q, float x, float y, float z)
{
	return   q.m11 * x*x + 2 * q.m12 * x*y + 2 * q.m13 * x*z + 2 * q.m14 * x + q.m22 * y*y
		+ 2 * q.m23 * y*z + 2 * q.m24 * y + q.m33 * z*z + 2 * q.m34 * z + q.m44;
}