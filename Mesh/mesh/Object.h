#ifndef OBJECT_H
#define OBJECT_H

#include "core.h"
#include "vector3.h"
#include "matrix34.h"

////////////////////////////////////////////////////////////////////////////////

class Object {
public:
	virtual void Update() = 0;
	virtual void Reset() = 0;
	virtual void Draw() = 0;
	virtual void GetMinMax(Vector3& min, Vector3& max) = 0;

	void Rotate(float x, float y)
	{
		angleX += x;
		angleY += y;

		if (angleX > 360) angleX -= 360;
		if (angleY > 360) angleY -= 360;

		rotationMtx.Identity();
		Matrix34 xAxis, yAxis;
		xAxis.MakeRotateX(angleX);
		yAxis.MakeRotateY(angleY);
		rotationMtx.Dot(xAxis, yAxis);
	}

	void Translate(float x, float y, float z)
	{
		translationMtx.Identity();
		translationMtx.MakeTranslate(Vector3(x, y, z));
	}

	Matrix34 worldMtx;

protected:
	// Constants
	Vector3 position;
	float angleX = 0.0f;
	float angleY = 0.0f;

	// Variables
	Matrix34 rotationMtx;
	Matrix34 translationMtx;
	Matrix34 localMtx;
};

////////////////////////////////////////////////////////////////////////////////

#endif
