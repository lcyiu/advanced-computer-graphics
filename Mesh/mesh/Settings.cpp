#include "Settings.h"

bool Settings::_allowFins = true;
bool Settings::_allowQuadrics = true;
bool Settings::_displayNormals = false;
bool Settings::_displayWireFrame = false;
bool Settings::_useCallList = false;
float Settings::_t = 0.0f;
bool Settings::_greyColor = false;