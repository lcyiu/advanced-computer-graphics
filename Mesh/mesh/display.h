////////////////////////////////////////
// display.h
////////////////////////////////////////

#ifndef CSE169_DISPLAY_H
#define CSE169_DISPLAY_H

#include "core.h"
#include "camera.h"
#include "cube.h"
#include "TriangleMesh.h"

////////////////////////////////////////////////////////////////////////////////



class Display 
{
public:
	Display(int argc, char **argv);
	~Display();

	void Update();
	void Reset();
	void Draw();
	void Quit();

	void LoadScene(int argc, char **argv);
	void ComputeFPS();

	// Event handlers
	void Resize(int x,int y);
	void Keyboard(int key,int x,int y);
	void MouseButton(int btn,int state,int x,int y);
	void MouseMotion(int x,int y);

private:
	// Window management
	int WindowHandle;
	int WinX,WinY;

	// Input
	bool LeftDown,MiddleDown,RightDown;
	int MouseX,MouseY;

	// Components
	Camera Cam;
	SpinningCube* cube;
	TriangleMesh* mesh;

	//fps
	float curFrame = 0;
	int frameCount = 0;
	float lastFrameTime = 0.0f;
};

////////////////////////////////////////////////////////////////////////////////

#endif
