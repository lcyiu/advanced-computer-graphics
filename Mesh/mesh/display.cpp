////////////////////////////////////////
// display.cpp
////////////////////////////////////////

#include <ctime>
#include "OFFModelLoader.h"
#include "display.h"
#include "token.h"
#include "simplifyMesh.h"

using namespace std;

#define WINDOWTITLE	"Mesh Simplification"

// Output Message
////////////////////////////////////////////////////////////////////////////////

char* renderDefault = "Rendering default scene\n";
char* tooManyArgs = "Too many arguments.\n";
char* tooFewArgs = "Too few arguments.\n";
char* usage = "Usage : mesh.exe filename\n";

////////////////////////////////////////////////////////////////////////////////

GLuint meshDL;
int totalFace = 0;
bool collapsing = false;
bool splitting = false;
EdgeCollapseData* ecd;
bool animationDone = false;
bool obtainedData = false;
float tx = 0.01f;
float t = 0.0f;
Vector3 v0fromPos, v1fromPos, v0ToPos, v1ToPos;
bool needCollapse = false;
bool manualSpeed = false;

Display::Display(int argc, char **argv)
{
	LoadScene(argc, argv);

	WinX = 800;
	WinY = 600;
	LeftDown = MiddleDown = RightDown = false;
	MouseX = MouseY = 0;

	// Create the window
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(WinX, WinY);
	glutInitWindowPosition(0, 0);
	WindowHandle = glutCreateWindow(WINDOWTITLE);
	glutSetWindowTitle(WINDOWTITLE);
	glutSetWindow(WindowHandle);

	// Background color
	glClearColor(0.5f, 0.5f, 0.5f, 1.);

	//enable depth test
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDepthRange(0.0f, 1.0f);

	//light Setup
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glShadeModel(GL_SMOOTH);
	//glEnable(GL_COLOR_MATERIAL);

	GLfloat ambientLight[] = { 0.1f, 0.1f, 0.1f, 1.0f };
	GLfloat diffuseLight[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat specularLight[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat position[] = { 0.0f, 10.0f, 10.0f, 0.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
	glLightfv(GL_LIGHT0, GL_POSITION, position);

	// Initialize components
	Cam.SetAspect(float(WinX) / float(WinY));
	glLineWidth(3.0f);

	if (mesh)
	{
		//use gl call list for faster rendering
		//meshDL = glGenLists(1);
		//glNewList(meshDL, GL_COMPILE);
		//mesh->Draw();
		//glEndList();
	}
}

////////////////////////////////////////////////////////////////////////////////

Display::~Display() 
{
	delete cube;
	delete mesh;

	glFinish();
	glutDestroyWindow(WindowHandle);
}

////////////////////////////////////////////////////////////////////////////////

void Display::Update() 
{
	// Update the components in the world
	Cam.Update();

	if (obtainedData && !animationDone)
	{
		//interpolate
		t += tx;
		if (t >= 1)
		{
			t = 1.0f;
			animationDone = true;
			obtainedData = false;
		}
		mesh->vertexList[ecd->v[0].first]->position = t * v0ToPos + (1 - t) * v0fromPos;
		mesh->vertexList[ecd->v[1].first]->position = t * v1ToPos + (1 - t) * v1fromPos;
	}
	else
	{
		if (needCollapse)
		{
			SimplifyMesh::Collapse(mesh);
			needCollapse = false;
			animationDone = false;
		}

		if (collapsing || splitting)
		{
			if (!manualSpeed)
			{
				if (mesh->face3List.size() < 50)
				{
					tx = 0.1f;
				}
				else if (mesh->face3List.size() < 200)
				{
					tx = 0.4f;
				}
				else
				{
					tx = 1;
				}
			}

			if (collapsing)
			{
				if (tx == 1)
				{
					int times = (int)ceil(1.0f / curFrame * mesh->face3List.size() * 0.25f);
					for (int i = 0; i < times; i++)
					{
						SimplifyMesh::Collapse(mesh);
					}

					if (SimplifyMesh::collapsedStack.empty())
					{
						collapsing = false;
						obtainedData = false;
						printf("All edges has been collapsed\t\r");
					}
				}
				else
				{
					ecd = SimplifyMesh::getCollapse();
					if (!ecd)
					{
						collapsing = false;
						obtainedData = false;
						printf("All edges has been collapsed\t\r");
					}
					else
					{
						t = 0;
						obtainedData = true;
						v0fromPos = mesh->vertexList[ecd->v[0].first]->position;
						v1fromPos = mesh->vertexList[ecd->v[1].first]->position;
						v0ToPos = v1ToPos = ecd->newV.second;
						needCollapse = true;
					}
				}
			}
			
			if (splitting)
			{
				if (animationDone)
				{
					animationDone = false;
				}

				if (tx == 1)
				{
					int times = (int)ceil(1.0f / curFrame *  mesh->face3List.size() * 0.25f);
					for (int i = 0; i < times; i++)
					{
						SimplifyMesh::Split(mesh, false);
					}

					if (SimplifyMesh::splitedStack.empty())
					{
						collapsing = false;
						obtainedData = false;
						printf("All edges has been splitted\t\r");
					}
				}
				else
				{
					ecd = SimplifyMesh::getSplit();
					SimplifyMesh::Split(mesh, true);
					if (!ecd)
					{
						splitting = false;
						obtainedData = false;
						printf("All edges has been splitted\t\r");
					}
					else
					{
						t = 0;
						obtainedData = true;
						v0fromPos = v1fromPos = ecd->newV.second;
						v0ToPos = ecd->v[0].second;
						v1ToPos = ecd->v[1].second;
					}
				}
			}
		}
	}

	if (mesh)
	{
		mesh->Update();
	}
	else
	{
		cube->Update();
	}

	// Tell glut to re-display the scene
	glutSetWindow(WindowHandle);
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////

void Display::Reset() {

	if (mesh)
	{
		mesh->Reset();
	}
	else
	{
		cube->Reset();
	}

	Cam.Reset();
	Cam.SetAspect(float(WinX)/float(WinY));
}

////////////////////////////////////////////////////////////////////////////////

void Display::Draw() {
	// Begin drawing scene
	glViewport(0, 0, WinX, WinY);
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Draw components
	Cam.Draw();		// Sets up projection & viewing matrices
	if (mesh)
	{
		//glPushMatrix();
		//glLoadMatrixf(mesh->worldMtx);
		//glCallList(meshDL);
		//glPopMatrix();
		glPushMatrix();
		glLoadMatrixf(mesh->worldMtx);
		mesh->Draw();
		glPopMatrix();

	}
	else
	{
		cube->Draw();
	}	
	//glPushMatrix();
	//glLoadMatrixf(mesh->worldMtx);
	//mesh->Draw();
	//glPopMatrix();

	ComputeFPS();

	// Finish drawing scene
	glFinish();
	glutSwapBuffers();
}

////////////////////////////////////////////////////////////////////////////////

void Display::Quit() {
	glFinish();
	glutDestroyWindow(WindowHandle);

	exit(0);
}

////////////////////////////////////////////////////////////////////////////////

void Display::Resize(int x, int y) {
	WinX = x;
	WinY = y;
	Cam.SetAspect(float(WinX)/float(WinY));
}

////////////////////////////////////////////////////////////////////////////////
EdgeCollapseData* e;

void Display::Keyboard(int key, int x, int y) {

	if (mesh)
	{
		switch (key) {
		case 0x1b:		// Escape
			Quit();
			break;
		case 'r':
			Reset();
			break;
		case 'z':
			Cam.SetDistance(Cam.GetDistance()*(1.0f + 0.1f));
			break;
		case 'x':
			Cam.SetDistance(Cam.GetDistance()*(1.0f - 0.1f));
			break;
		case 'c':
			splitting = false;
			collapsing = !collapsing;
			printf("Collapsing edges\t\t\t\t\r");
			break;
		case 's':
			splitting = !splitting;
			collapsing = false;
			printf("Splitting edges\t\t\t\t\r");
			break;
		case 'w':
			Settings::_displayWireFrame = !Settings::_displayWireFrame;
			break;
		case 'n':
			Settings::_displayNormals = !Settings::_displayNormals;
			break;
		case '=':
			tx += 0.01f;
			tx = tx > 1 ? 1 : tx;
			printf("Rate of geomorph tx: %.2f\t\r", tx);
			break;
		case '-':
			tx -= 0.01f;
			tx = tx < 0 ? 0 : tx;
			printf("Rate of geomorph tx: %.2f\t\r", tx);
			break;
		case '1':
			tx = 1;
			printf("Rate of geomorph tx: %.2f\t\r", tx);
			break;
		case '0':
			tx = 0;
			printf("Rate of geomorph tx: %.2f\t\r", tx);
			break;
		case 'm':
			manualSpeed = !manualSpeed;
			printf("Manual Speed: %s\t\t\r", manualSpeed ? "true" : "false");
			break;
		case 'g':
			Settings::_greyColor = !Settings::_greyColor;
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::MouseButton(int btn, int state, int x, int y) {
	if(btn==GLUT_LEFT_BUTTON) {
		LeftDown = (state==GLUT_DOWN);
	}
	else if(btn==GLUT_MIDDLE_BUTTON) {
		MiddleDown = (state==GLUT_DOWN);
	}
	else if(btn==GLUT_RIGHT_BUTTON) {
		RightDown = (state==GLUT_DOWN);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::MouseMotion(int nx, int ny) {
	int dx = nx - MouseX;
	int dy = -(ny - MouseY);

	MouseX = nx;
	MouseY = ny;

	// Move camera
	// NOTE: this should really be part of Camera::Update()
	if(LeftDown) 
	{
		const float rate = 0.005f;

		if (mesh)
		{
			mesh->Rotate(-dy*rate, dx*rate);
		}
		else
		{
			cube->Rotate(-dy*rate, dx*rate);
		}
	}

	if(RightDown) 
	{
		const float rate = 0.1f;
		Cam.SetDistance(Cam.GetDistance() + dy * rate);
		Cam.SetDistance(Cam.GetDistance() + -dx * rate);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::LoadScene(int argc, char **argv)
{
	// Scene
	if (argc == 2)
	{
		printf("\n                          ****** LOAD SCENE ******                             \n\n");
		clock_t beginT = clock();
		mesh = OFFModelLoader::loadFile(argv[1]);
		if (mesh == NULL)
		{
			printf(renderDefault);
		}
		else
		{
			mesh->PreCalc();

			//adjust camera position
			printf("Display::LoadScene()- Adjusting Camera Position For Showing Model...\n");
			Vector3 min, max;
			mesh->GetMinMax(min, max);
			float maxDistanceXY = 1.5f * fmax(fmax(abs(min.x), abs(max.x)), fmax(abs(min.y), abs(max.y)));
			float finalDistance = fmax(1.5f * max.z, maxDistanceXY / (float)tan(30.0 * M_PI / 180.0));
			Cam.SetDistance(finalDistance);

			clock_t endT = clock();
			printf("Display::LoadScene()- Object Setup Time: %.2fs\n", (float)(endT - beginT) / CLOCKS_PER_SEC);
			totalFace = (int) mesh->face3List.size();
			SimplifyMesh::Initiate(mesh);
		}
	}
	else if (argc > 2)
	{
		printf(tooManyArgs);
		printf(renderDefault);
		printf(usage);
	}
	else
	{
		printf(tooFewArgs);
		printf(renderDefault);
		printf(usage);
	}

	cube = new SpinningCube();
}

////////////////////////////////////////////////////////////////////////////////

void Display::ComputeFPS()
{
	// post: compute frames per second and display in window's title bar
	frameCount++;
	int currentTime = glutGet(GLenum(GLUT_ELAPSED_TIME));
	if (currentTime - lastFrameTime > 1000)
	{
		char s[16];
		sprintf(s, "FPS: %4.2f",
			frameCount * 1000.0 / (currentTime - lastFrameTime));
		glutSetWindowTitle(s);

		curFrame = frameCount * 1000.0 / (currentTime - lastFrameTime);

		lastFrameTime = (float)currentTime;
		frameCount = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////
