////////////////////////////////////////
// cube.h
////////////////////////////////////////

#ifndef CSE169_CUBE_H
#define CSE169_CUBE_H

#include "Object.h"

////////////////////////////////////////////////////////////////////////////////

class SpinningCube : public Object {
public:
	SpinningCube();

	void Update();
	void Reset();
	void Draw();
	void GetMinMax(Vector3& min, Vector3& max){};

private:
	// Constants
	float Size;
	Vector3 Axis;

	// Variables
	float Angle;
};

////////////////////////////////////////////////////////////////////////////////

/*
SpinningCube is an example of a basic animating object. It can be used as a
pattern for creating more complex objects.
*/

#endif
