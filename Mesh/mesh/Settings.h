#ifndef SETTINGS_H
#define SETTINGS_H

class Settings
{
public: 
	static bool _allowFins;
	static bool _allowQuadrics;
	static bool _displayNormals;
	static bool _displayWireFrame;
	static bool _useCallList;
	static bool _greyColor;
	static float _t;
};

#endif