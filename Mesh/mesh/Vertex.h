#ifndef VERTEX_H
#define VERTEX_H

#include "vector3.h"
#include "Matrix4x4.h"

////////////////////////////////////////////////////////////////////////////////

class Vertex {
public:
	Vertex() {};

	Vertex(Vector3 pos) 
	{
		position = pos;
	};

	Vertex(float x, float y, float z)
	{
		position = Vector3(x, y, z);
	};

	Vector3 position;
	Vector3 normal;
	Matrix4x4 quadricMtx;
};

////////////////////////////////////////////////////////////////////////////////

#endif
