#ifndef TRIANGLEMESH_H
#define TRIANGLEMESH_H

#include <list>
#include <unordered_map>
#include "Triangle.h"
#include "Object.h"
#include "Vertex.h"

////////////////////////////////////////////////////////////////////////////////

typedef list<int> FaceAdjList;
typedef unordered_map<int, Triangle*> Face3ListMap;
typedef unordered_map<int, Vertex*> VertexListMap;
typedef unordered_map<int, FaceAdjList> VertexFaceAdjListMap;

struct EdgeCollapseData
{
	//First, Add back All removed faces
	//Then, Add face to adj list of removed faces ( do not add face to newVIndex)
	//Then, Add back v1 and v2
	//Last, recalculate all affected face and vertices including removed ones
	pair<int, Vector3> newV;
	Vector3 newVN;
	pair<int, Vector3> v[2];
	int vDataSize[2];
	int removedFaceSize;
	//pair<int, int>* vData[2];
	//pair<int, Triangle*>* removedFace;
	list<pair<int, int>> vData[2];	//list contain all changed faces with first int = face# and second = which vertex of the face 0 for v1, 1 for v2, 2 for v3
	list<pair<int, Triangle*>> removedFace;
	//list<int> affectedVertex;
	//list<int> affectedFace;
};


class TriangleMesh : public Object {
public:
	TriangleMesh(){};

	void Update(){ worldMtx.Dot(rotationMtx, translationMtx); };
	void Reset(){ rotationMtx.Identity(); angleX = angleY = 0.0f; };
	void Draw();
	void GetMinMax(Vector3& min, Vector3& max);

	void Collapse(EdgeCollapseData* );
	EdgeCollapseData* Collapse(int v1, int v2, Vector3& dstV);
	void Split(EdgeCollapseData* ecd, bool atSplitPos);

	void PreCalc();

	int faceIncex = 0;
	int vertexIndex = 0;

	Face3ListMap face3List;
	VertexFaceAdjListMap vertexFaceAdjList;
	VertexListMap vertexList;

private:
	void CalcTriangleNormal(Triangle* face);
	void CalcVertexNormal(pair<const int, Vertex*>& it);

	void ChangeFaceVertices(int v, EdgeCollapseData* ecd, int vData);
	void DegenConcideFace(int v, EdgeCollapseData* ecd);
	void DegenMirrorPolygon(int faceAdjIndex);
	void RemoveTriangleFromList(int tIndex, Triangle* t);

	bool CheckConcideVertices(int v1, int v2, int v3);
	bool CheckSamePolygon(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 v5, Vector3 v6);
};

////////////////////////////////////////////////////////////////////////////////

#endif
