////////////////////////////////////////
// display.cpp
////////////////////////////////////////
/**
* OS specific
**/
#define OS_UNIX 1
#define OS_WIN 2

#ifdef USE_UNIX
#   define OS OS_UNIX
#else
#   define OS OS_WIN
#endif

#if OS == OS_WIN
#   define _CRT_SECURE_NO_WARNINGS
#   include <io.h>
#   include <fcntl.h>
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "image.h"
#include "bmp.h"

#include <iostream>
#include <fstream>
#include <string.h>
#include "display.h"
#include "ErrorMessage.h"

#define WINDOWTITLE	"CSE169 PA1"

////////////////////////////////////////////////////////////////////////////////
int WindowHandle;

GLUI* glui, *glui_panel;
GLUI_EditText * glui_fileSavePath;
GLUI_Button * glui_save, *glui_add, *glui_iq;
GLUI_Rollout * glui_file_Rollout;
GLUI_Rollout * glui_output_Rollout;
GLUI_Rollout * glui_arg_Rollout;
GLUI_FileBrowser * fb;
GLUI_List        *glui_arg_list;
GLUI_Panel       *glui_arg_panel;
GLUI_TextBox* glui_consoleOutput;
GLUI_EditText *glui_newarg_text;

int arg_index = 0;
string argList[100];
string savePath = "";
string outputMsg="Please Open a File...\n";

Image* img;
Image* currentImg;

bool imageUpdated = false;
int imageQuilting = 0;

void processImage(int argc, char **argv, Image*& input);
void processTexture(Image*& input, int row, int col, int size);

struct Pixel2
{
	unsigned char r, g, b;
	Pixel2(unsigned char ir, unsigned char ig, unsigned char ib) { set(ir, ig, ib); }
	Pixel2() : r(0), g(0), b(0) {}
	void set(unsigned char ir, unsigned char ig, unsigned char ib) { r = ir; g = ig; b = ib; }
};

Pixel2* pixels;

/**
* SetBinaryIOMode
*
* In WindowsNT by default stdin and stdout are opened as text files.
* This code opens both as binary files.
**/
static void SetBinaryIMode(void)
{
#if OS == OS_WIN
	int result;

	result = _setmode(_fileno(stdin), _O_BINARY);
	if (result == -1)
	{
		perror("Cannot set stdin binary mode");
		exit(EXIT_FAILURE);
	}

#endif
}

static void SetBinaryOMode(void)
{
#if OS == OS_WIN
	int result;

	result = _setmode(_fileno(stdout), _O_BINARY);
	if (result == -1)
	{
		perror("Cannot set stdout binary mode");
		exit(EXIT_FAILURE);
	}
#endif
}

void drawScanline(int y)
{
	glRasterPos2f(-1, 1 - (2 * y) / (currentImg->height * 1.0));
	glDrawPixels(currentImg->width, 1, GL_RGB, GL_UNSIGNED_BYTE, &pixels[y*currentImg->width]);
}

void outputMessage(string str)
{
	outputMsg += str;
	glui_consoleOutput->set_text(&outputMsg[0]);
	glui_consoleOutput->curr_line;
}

void Tokenize(const string& str,
	vector<string>& tokens,
	const string& delimiters = " ")
{
	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos)
	{
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}
}

void runProcessImage()
{
	string args = "";
	vector<string> token;
	int argc = 0;
	char** argv;

	for (int i = 0; i < arg_index; i++)
	{
		args += argList[i];
		args += " ";
	}

	Tokenize(args, token); 
	
	if (token.size() > 0)
	{
		argc = token.size();
		argv = new char*[argc];
	}

	for (int i = 0; i < argc; i++)
	{
		argv[i] = &token.at(i)[0];
	}

	delete currentImg;
	currentImg = 0;
	currentImg = new Image(*img);

	if (argc > 0)
	{
		processImage(argc, argv, currentImg);
	}

	if (pixels)
	{
		delete[] pixels;

	}
	pixels = 0;
	pixels = new Pixel2[currentImg->width * currentImg->height];
	memset(pixels, 0, currentImg->width * currentImg->height * sizeof(Pixel2));

	for (int i = 0; i < currentImg->num_pixels; i++)
	{
		pixels[i].set(currentImg->pixels[i].r, currentImg->pixels[i].g, currentImg->pixels[i].b);
	}
}

void pointer_cb(GLUI_Control* control)
{
	if (control->get_id() == OPENPANELID && img) {
		/****** Make command line window ******/
		if (glui_panel)
		{
			glui_panel->show();
			glui_newarg_text->set_text("");
		}
		else
		{
			glui_panel = GLUI_Master.create_glui("New Arguments:",
				0, 50, 500);

			glui_newarg_text = new GLUI_EditText(glui_panel, "Enter Arguments: ");
			glui_newarg_text->set_w(400);  /** Widen 'command line' control **/

			GLUI_Panel *panel = new GLUI_Panel(glui_panel, "", GLUI_PANEL_NONE);
			new GLUI_Button(panel, "Close", CANCELID, pointer_cb);
			new GLUI_Column(panel, false);
			new GLUI_Button(panel, "Add", ADDID, pointer_cb);

			glui_panel->set_main_gfx_window(WindowHandle);

			control->disable();
		}
	}
	else if (control->get_id() == CANCELID) {
		glui_panel->hide();
		glui_add->enable();
	}
	else if (control->get_id() == ADDID) {
		argList[arg_index] = glui_newarg_text->get_text();
		glui_arg_list->add_item(arg_index, glui_newarg_text->get_text());
		arg_index++;
		glui_panel->hide();
		glui_add->enable();

		
		runProcessImage();
		imageUpdated = true;
	}

}

void ImageProcessingCallBack(int id)
{
	string path;
	FILE* f;

	if (id == SAVEID)
	{
		if (img)
		{
			f = freopen(&savePath[0], "w", stdout);

			if (f)
			{
				SetBinaryOMode();
				BMPWriteImage(currentImg, stdout);
				fclose(stdout);

				freopen("CON", "w", stdout);
				outputMessage("Saved Successfully!!\n");
			}
			else
			{
				freopen("CON", "w", stdout);
				outputMessage("Invalid Image Path: " + path + "\n");
				outputMessage("Example Path: Images/flower.bmp\n");
			}
		}
		else
		{
			outputMessage("Input Image Has Not Been Yet!!\n");
		}

	}
	else if (id == FILEBROWSERID)
	{
		path = fb->get_file();
		if (strcmp( &path.substr(path.find_last_of(".") + 1)[0], "bmp") == 0) {
			f = freopen(&path[0], "r", stdin);

			if (f)
			{
				SetBinaryIMode();

				if (img != NULL)
				{
					delete img;
				}

				img = BMPReadImage(f);

				currentImg = new Image(*img);

				if (pixels)
				{
					delete[] pixels;

				}
				pixels = 0;
				pixels = new Pixel2[img->width * img->height];
				memset(pixels, 0, img->width * img->height * sizeof(Pixel2));

				for (int i = 0; i < img->num_pixels; i++)
				{
					pixels[i].set(img->pixels[i].r, img->pixels[i].g, img->pixels[i].b);
				}

				fclose(stdin);
				outputMessage("\rOpen Successfully!!\n");
				imageUpdated = true;
			}
			else
			{
				outputMessage("\rInvalid Image Path: " + path + "\n");
				outputMessage("\rExample Path: Images/flower.bmp\n");
			}
			freopen("CON", "r", stdin);
		}
	}
	else if (id == DELETEONEID && img)
	{
		if (arg_index > 0)
		{
			glui_arg_list->delete_item(--arg_index);
			runProcessImage();
			imageUpdated = true;
		}

	}
	else if (id == CLEARALLID && img)
	{
		arg_index = 0;
		glui_arg_list->delete_all();
		runProcessImage();
		imageUpdated = true;
	}
};

Display::Display()
{
	WinX = 800;
	WinY = 600;
	LeftDown = MiddleDown = RightDown = false;
	MouseX = MouseY = 0;

	// Create the window
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(WinX, WinY);
	WindowHandle = glutCreateWindow(WINDOWTITLE);
	glutSetWindowTitle(WINDOWTITLE);
	glutSetWindow(WindowHandle);

	/*** Create the side subwindow ***/
	////////////////////////////////////////////////////////////////////////////////
	//GLUI SETUP
	////////////////////////////////////////////////////////////////////////////////

	glui = GLUI_Master.create_glui("Image.exe"); 

	//FILE
	glui_file_Rollout = new GLUI_Rollout(glui, "File");

	fb = new GLUI_FileBrowser(glui_file_Rollout, "Open Path", true, FILEBROWSERID, ImageProcessingCallBack);

	glui_fileSavePath = new GLUI_EditText(glui_file_Rollout, "Save Path: ", savePath);
	glui_fileSavePath->set_w(200);
	glui_save = new GLUI_Button(glui_file_Rollout, "Save", SAVEID, ImageProcessingCallBack);

	new GLUI_Separator(glui);
	glui_arg_Rollout = new GLUI_Rollout(glui, "Arguments");

	glui_arg_list = new GLUI_List(glui_arg_Rollout, "Current");
	glui_add = new GLUI_Button(glui_arg_Rollout, "Add", OPENPANELID, pointer_cb);
	new GLUI_Button(glui_arg_Rollout, "Remove Last", DELETEONEID, ImageProcessingCallBack);
	new GLUI_Button(glui_arg_Rollout, "Clear All", CLEARALLID, ImageProcessingCallBack);

	glui_output_Rollout = new GLUI_Rollout(glui, "Output");
	glui_consoleOutput = new GLUI_TextBox(glui_output_Rollout, true);
	glui_consoleOutput->set_text(&outputMsg[0]);
	glui_consoleOutput->set_w(200);
	glui_consoleOutput->disable();
	glui_consoleOutput->scrollbar->enable();

	// Background color
	glClearColor(0., 0., 0., 1.);

	// Initialize components
	Cam.SetAspect(float(WinX) / float(WinY));
}

////////////////////////////////////////////////////////////////////////////////

Display::~Display() {
	glFinish();
	glutDestroyWindow(WindowHandle);
}

////////////////////////////////////////////////////////////////////////////////

void Display::Update() {

	if (currentImg != NULL)
	{
		if (currentImg->width != WinX || currentImg->height != WinY)
		{
			glutSetWindow(WindowHandle);
			glutReshapeWindow(currentImg->width, currentImg->height);
		}
	}

	glutSetWindow(WindowHandle);
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////

void Display::Reset() {
	Cam.Reset();
	Cam.SetAspect(float(WinX) / float(WinY));
}

////////////////////////////////////////////////////////////////////////////////

void Display::Draw() {
	// Begin drawing scene
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDrawBuffer(GL_FRONT);

	if (currentImg && imageUpdated)
	{
		for (int i = 0; i < currentImg->height; i++)
			drawScanline(i);
	}

	// Finish drawing scene
	glFinish();
}

////////////////////////////////////////////////////////////////////////////////

void Display::Quit() {
	glFinish();
	glutDestroyWindow(WindowHandle);
	exit(0);
}

////////////////////////////////////////////////////////////////////////////////

void Display::Resize(int x, int y) {
	WinX = x;
	WinY = y;
	glViewport(0, 0, WinX, WinY);
	Cam.SetAspect(float(WinX) / float(WinY));
}

////////////////////////////////////////////////////////////////////////////////

void Display::Keyboard(int key, int x, int y) {
	switch (key) {
	case 0x1b:		// Escape
		Quit();
		break;
	case 'r':
		Reset();
		break;
	case 'z':
		break;
	case 'x':
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::MouseButton(int btn, int state, int x, int y) {
	if (btn == GLUT_LEFT_BUTTON) {
		LeftDown = (state == GLUT_DOWN);
	}
	else if (btn == GLUT_MIDDLE_BUTTON) {
		MiddleDown = (state == GLUT_DOWN);
	}
	else if (btn == GLUT_RIGHT_BUTTON) {
		RightDown = (state == GLUT_DOWN);
	}
}

////////////////////////////////////////////////////////////////////////////////

void Display::MouseMotion(int nx, int ny) {
	int dx = nx - MouseX;
	int dy = -(ny - MouseY);

	MouseX = nx;
	MouseY = ny;

	// Move camera
	// NOTE: this should really be part of Camera::Update()
	if (LeftDown) {
	}
	if (RightDown) {
	}
}

////////////////////////////////////////////////////////////////////////////////

/**
* ShowUsage
**/
static char options[] =
"-help\n"
"-brightness <factor>\n"
"-contrast <factor>\n"
"-saturation <factor>\n"
"-gamma <gamma>\n"
"-crop <x> <y> <width> <height>\n"
"-quantize <nbits>\n"
"-randomDither <nbits>\n"
"-FloydSteinbergDither <nbits>\n"
"-blur <maskSize>\n"
"-sharpen\n"
"-edgeDetect <threshold>\n"
"-size <sx> <sy>\n"
"-shift <tx> <ty>\n"
"-fun <amplitude> <scale>\n"
"-composite <bottomMaskFileName> <topImageFileName> <topMaskFileName>\n"
"-morph <dstImageFileName> <correspondencesFileName> <t>\n"
"-sampling <method no>\n"
"-imageQuilting <col> <row> <size>\n"
"-imageTiling <col> <row> <size>\n"
;

static void ShowUsage(void)
{
	string msg = options;
	msg += "\n";
	outputMessage(msg);
}

/**
* CheckOption
**/
static int CheckOption(char *option, int argc, int minargc)
{
	if (argc < minargc)
	{
		string msg = "Too few arguments for ";
		msg += option;
		msg += "\n";
		outputMessage(msg);
		ShowUsage();
		return 0;
	}
	return 1;
}


/**
* main
**/
void processImage(int argc, char **argv, Image*& input)
{
	Image *img = new Image(*input);

	// look for help
	for (int i = 0; i < argc; i++) {
		if (!strcmp(argv[i], "-help")) {
			ShowUsage();
			return;
		}
	}

	if (argc == 0)
	{
		return;
	}

	// parse arguments
	while (argc > 0)
	{
		if (**argv == '-')
		{
			if (!strcmp(*argv, "-brightness"))
			{
				double factor;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				factor = atof(argv[1]);
				if (factor < 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-brightness", factor);
					outputMessage(buf);
					return;
				}

				img->Brighten(factor);
				argv += 2, argc -= 2;
			}

			else if (!strcmp(*argv, "-contrast"))
			{
				double factor;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				factor = atof(argv[1]);
				img->ChangeContrast(factor);
				argv += 2, argc -= 2;
			}

			else if (!strcmp(*argv, "-saturation"))
			{
				double factor;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				factor = atof(argv[1]);
				img->ChangeSaturation(factor);
				argv += 2, argc -= 2;
			}

			else if (!strcmp(*argv, "-gamma"))
			{
				double factor;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				factor = atof(argv[1]);
				if (factor < 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-gamma", factor);
					outputMessage(buf);
					return;
				}

				img->ChangeGamma(factor);
				argv += 2, argc -= 2;
			}

			else if (!strcmp(*argv, "-crop"))
			{
				int x, y, w, h;
				if (!CheckOption(*argv, argc, 5))
				{
					return;
				}

				x = atoi(argv[1]);
				y = atoi(argv[2]);
				w = atoi(argv[3]);
				h = atoi(argv[4]);

				if ((x < 0 || x > img->width - 1) || (y < 0 || y > img->height - 1) || (w <= 0 || x + w > img->width) || (h <= 0 || y + h > img->height))
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputOutOfRange[0], "-crop");
					outputMessage(buf);
					return;
				}

				Image *dst = img->Crop(x, y, w, h);
				delete img;
				img = dst;

				argv += 5, argc -= 5;
			}
			else if (!strcmp(*argv, "-quantize"))
			{
				int nbits;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				nbits = atoi(argv[1]);
				if (nbits < 1 || nbits > 8)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputOutOfRange[0], "-quantize");
					outputMessage(buf);
					return;
				}

				img->Quantize(nbits);
				argv += 2, argc -= 2;
			}

			else if (!strcmp(*argv, "-randomDither"))
			{
				int nbits;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				nbits = atoi(argv[1]);
				if (nbits < 1 || nbits > 8)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputOutOfRange[0], "-randomDither");
					outputMessage(buf);
					return;
				}

				img->RandomDither(nbits);
				argv += 2, argc -= 2;
			}
			else if (!strcmp(*argv, "-blur"))
			{
				int n;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				n = atoi(argv[1]);
				if (n <= 1 || n % 2 == 0 || n > img->width || n > img->height)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::blurInputInvalid[0], "-blur", n);
					outputMessage(buf);
					return;
				}

				img->Blur(n);
				argv += 2, argc -= 2;
			}

			else if (!strcmp(*argv, "-sharpen"))
			{
				img->Sharpen();
				argv++, argc--;
			}

			else if (!strcmp(*argv, "-edgeDetect"))
			{
				int factor;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				factor = atoi(argv[1]);
				if (factor < 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-edgeDetect", factor);
					outputMessage(buf);
					return;
				}

				img->EdgeDetect(factor);
				argv += 2, argc -= 2;
			}
			else if (!strcmp(*argv, "-FloydSteinbergDither"))
			{
				int nbits;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				nbits = atoi(argv[1]);
				if (nbits < 1 || nbits > 8)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputOutOfRange[0], "-FloydSteinbergDither");
					outputMessage(buf);
					return;
				}

				img->FloydSteinbergDither(nbits);
				argv += 2, argc -= 2;
			}
			else if (!strcmp(*argv, "-size"))
			{
				if (!CheckOption(*argv, argc, 3))
				{
					return;
				}

				int sizex = atoi(argv[1]);
				int sizey = atoi(argv[2]);
				if (sizex < 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-size", sizex);
					outputMessage(buf);
					return;
				}

				if (sizey < 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-size", sizey);
					outputMessage(buf);
					return;
				}

				Image *dst = img->Scale(sizex, sizey);
				delete img;

				img = dst;
				argv += 3, argc -= 3;
			}

			else if (!strcmp(*argv, "-shift"))
			{
				if (!CheckOption(*argv, argc, 3))
				{
					return;
				}

				double sx = atof(argv[1]);
				double sy = atof(argv[2]);

				img->Shift(sx, sy);
				argv += 3, argc -= 3;
			}
			else if (!strcmp(*argv, "-fun"))
			{
				if (!CheckOption(*argv, argc, 3))
				{
					return;
				}

				int amplitude = atoi(argv[1]);
				int wavelength = atoi(argv[2]);
				if (amplitude < 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-fun", amplitude);
					outputMessage(buf);
					return;
				}

				if (wavelength < 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-fun", wavelength);
					outputMessage(buf);
					return;
				}

				img->Fun(amplitude, wavelength);
				argv += 3, argc -= 3;
			}
			else if (!strcmp(*argv, "-sampling"))
			{

				int method;
				if (!CheckOption(*argv, argc, 2))
				{
					return;
				}

				method = atoi(argv[1]);
				img->SetSamplingMethod(method);
				argv += 2, argc -= 2;
			}
			else if (!strcmp(*argv, "-imageQuilting"))
			{
				if (!CheckOption(*argv, argc, 4))
				{
					return;
				}
				int col = atoi(argv[1]);
				int row = atoi(argv[2]);
				int size = atoi(argv[3]);
				if (col <= 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-imageQuilting", col);
					outputMessage(buf);
				}

				if (row <= 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-imageQuilting", row);
					outputMessage(buf);
				}

				if (size <= 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-imageQuilting", size);
					outputMessage(buf);
				}

				argv += 4, argc -= 4;
			}
			else if (!strcmp(*argv, "-imageTiling"))
			{
				if (!CheckOption(*argv, argc, 4))
				{
					return;
				}
				int col = atoi(argv[1]);
				int row = atoi(argv[2]);
				int size = atoi(argv[3]);
				if (col <= 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-imageTiling", col);
					outputMessage(buf);
				}

				if (row <= 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-imageTiling", row);
					outputMessage(buf);
				}

				if (size <= 0)
				{
					char buf[500];
					sprintf(buf, &ErrorMSG::inputIsNegative[0], "-imageTiling", size);
					outputMessage(buf);
				}

				argv += 4, argc -= 4;
			}
			else
			{
				char buf[500];
				sprintf(buf, "image: invalid option: %s\n", *argv);
				outputMessage(buf);
				ShowUsage();
				return;
			}
		}
		else {
			char buf[500];
			sprintf(buf, "image: invalid option: %s\n", *argv);
			outputMessage(buf);
			ShowUsage();
			return;
		}
	}

	delete input;
	input = 0;
	input = img;

	// done
	return;
}
