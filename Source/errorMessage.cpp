#include "errorMessage.h"

const string ErrorMSG::inputIsNegative = "image: Parameter for option %s : %f cannot be negative.\n";

const string ErrorMSG::inputOutOfRange = "image: Parameter for option %s : Out of Range.\n";

const string ErrorMSG::blurInputInvalid = "image: Parameter for option %s : %d has to be odd, greater than 1 and smaller than size of image.\n";

const string ErrorMSG::inputIsNotDivisible = "image: Parameter for option %s : Row/Col Not Divisible by size %d.\n";