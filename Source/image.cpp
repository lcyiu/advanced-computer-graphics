#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <vector>

#include "image.h"
#include "bmp.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include <stdlib.h>
#include <string.h>
#include <float.h>

//Utility
const double shapenFilter[3][3] = 
{
	{ -1, -2, -1 },
	{ -2, 19, -2 },
	{ -1, -2, -1 }
};

const double edgeDetectFilterH[3][3] =
{
	{ -1, 0, 1 },
	{ -2, 0, 2 },
	{ -1, 0, 1 }
};
const double edgeDetectFilterV[3][3] =
{
	{ 1, 2, 1 },
	{ 0, 0, 0 },
	{ -1, -2, -1 }
};


double noise()
{
	double range = .5;
	return ((double)rand() / (RAND_MAX)) - range;
}

void gaussianFilter(const double& sigma, double**& filter, int n, double& scale)
{
	int half = n / 2;
	double smallest = INT_MAX;
	scale = 0;
	double sigmaSQ = pow(sigma, 2.0);

	for (int i = 0; i < n; i++)
	{
		int y = (int)pow(i - half, 2.0);
		for (int j = 0; j < n; j++)
		{
			int x = (int)pow(j - half, 2.0);
			double result = 0.5 / M_PI / sigmaSQ * exp(-(x + y) / (2 * sigmaSQ));

			filter[i][j] = result;
			if (result < smallest)
			{
				smallest = result;
			}
		}
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			filter[i][j] = round(filter[i][j] / smallest);
			scale += filter[i][j];
		}
	}
}

double Luminance(int r, int g, int b)
{
	return (r * 76 + g * 150 + b * 29) >> 8;
}

Pixel QuantizePixel(int nbits, double bq2, double bq, Pixel p)
{
	int finalR = (int)floor(bq2 * floor(p.r * bq));
	int finalG = (int)floor(bq2 * floor(p.g * bq));
	int finalB = (int)floor(bq2 * floor(p.b * bq));

	p.r = finalR;
	p.g = finalG;
	p.b = finalB;

	return p;
}

/**
 * Image
 **/
Image::Image (int width_, int height_)
{
    assert(width_ > 0);
    assert(height_ > 0);

    width           = width_;
    height          = height_;
    num_pixels      = width * height;
    pixels          = new Pixel[num_pixels];
    sampling_method = IMAGE_SAMPLING_POINT;

    assert(pixels != NULL);
}


Image::Image (const Image& src)
{
    width           = src.width;
    height          = src.height;
    num_pixels      = width * height;
    pixels          = new Pixel[num_pixels];
    sampling_method = IMAGE_SAMPLING_POINT;

    assert(pixels != NULL);
    memcpy(pixels, src.pixels, src.width * src.height * sizeof(Pixel));
}

 
Image::~Image ()
{
    delete [] pixels;
    pixels = NULL;
}

/*
void Image::AddNoise (double factor)
{

}
*/

void Image::Brighten (double factor)
{
  /* Your Work Here  (section 3.2.1 of assignment)*/
	for (int i = 0; i < num_pixels; i++)
	{
		pixels[i] = pixels[i] * factor;
	}
}


void Image::ChangeContrast (double factor)
{
  /* Your Work Here (section 3.2.2) */
	int greyC = 0;

	//get average grey pixel
	for (int i = 0; i < num_pixels; i++)
	{
		greyC += pixels[i].Luminance();
	}

	greyC /= num_pixels;
	Pixel greyP(greyC, greyC, greyC);
	 
	for (int i = 0; i < num_pixels; i++)
	{
		pixels[i] = PixelLerp(greyP, pixels[i], factor);
	}	
}


void Image::ChangeSaturation(double factor)
{
  /* Your Work Here (section 3.2.3) */

	//get average grey pixel
	for (int i = 0; i < num_pixels; i++)
	{
		int greyC = pixels[i].Luminance();

		pixels[i] = PixelLerp(Pixel(greyC, greyC, greyC), pixels[i], factor);
	}
}

void Image::ChangeGamma(double factor)
{
  /* Your Work Here (section 3.2.4) */
	factor = 1.0 / factor;

	for (int i = 0; i < num_pixels; i++)
	{
		pixels[i].r = (pixels[i].r == 0 || pixels[i].r == 255) ? pixels[i].r : ComponentClamp((int)(255 * pow(pixels[i].r / 255.0 , factor)));
		pixels[i].g = (pixels[i].g == 0 || pixels[i].g == 255) ? pixels[i].g : ComponentClamp((int)(255 * pow(pixels[i].g / 255.0 , factor)));
		pixels[i].b = (pixels[i].b == 0 || pixels[i].b == 255) ? pixels[i].b : ComponentClamp((int)(255 * pow(pixels[i].b / 255.0 , factor)));
	}
}

Image* Image::Crop(int x, int y, int w, int h)
{
  /* Your Work Here (section 3.2.5) */
	Image* img = new Image(w, h);

	for (int yPos = y, i = 0; i < h; i++, yPos++)
	{
		for (int xPos = x, j = 0; j < w; j++, xPos++)
		{
			img->pixels[i * w + j] = pixels[yPos * width + xPos];
		}
	}

	return img;
}

/*
void Image::ExtractChannel(int channel)
{
  // For extracting a channel (R,G,B) of image.  
  // Not required for the assignment
}
*/

void Image::Quantize (int nbits)
{
  /* Your Work Here (Section 3.3.1) */
	int b = (int)pow(2.0, nbits);
	double bq = b / 256.0;
	double bq2 = (255.0 / (b - 1));

	for (int i = 0; i < num_pixels; i++)
	{
		pixels[i] = QuantizePixel(nbits, bq2, bq, pixels[i]);
	}
}


void Image::RandomDither (int nbits)
{
  /* Your Work Here (Section 3.3.2) */
	int b = (int)pow(2.0, nbits);
	double bq = b / 256.0;
	double bq2 = (255.0 / (b - 1));
	double n;

	for (int i = 0; i < num_pixels; i++)
	{
		n = noise();
		int finalR = ComponentClamp((int)floor(bq2 * floor(pixels[i].r * bq + n)));
		int finalG = ComponentClamp((int)floor(bq2 * floor(pixels[i].g * bq + n)));
		int finalB = ComponentClamp((int)floor(bq2 * floor(pixels[i].b * bq + n)));

		pixels[i].SetClamp(finalR, finalG, finalB);
	}
}


/* Matrix for Bayer's 4x4 pattern dither. */
/* uncomment its definition if you need it */

/*
static int Bayer4[4][4] =
{
    {15, 7, 13, 5},
    {3, 11, 1, 9},
    {12, 4, 14, 6},
    {0, 8, 2, 10}
};


void Image::OrderedDither(int nbits)
{
  // For ordered dithering
  // Not required for the assignment
}

*/

/* Error-diffusion parameters for Floyd-Steinberg*/
const double
    ALPHA = 7.0 / 16.0,
    BETA  = 3.0 / 16.0,
    GAMMA = 5.0 / 16.0,
    DELTA = 1.0 / 16.0;

void Image::FloydSteinbergDither(int nbits)
{
  /* Your Work Here (Section 3.3.3) */
	int b = (int)pow(2.0, nbits);
	double bq = b / 256.0;
	double bq2 = (255.0 / (b - 1));

	for (int h = 0; h < height; h++)
	{
		for (int w = 0; w < width; w++)
		{
			int i = h * width + w;
			Pixel finalPixel = QuantizePixel(nbits, bq2, bq, pixels[i]);
			PixelDif Error = pixels[i] - finalPixel;

			int rightW = (w + 1 == width) ? 0 : w + 1,
				rightH = h,
				bottomLeftW = (w - 1 < 0) ? width - 1 : w - 1,
				bottomLeftH = (h + 1 == height) ? 0 : h + 1,
				bottomMidW = w,
				bottomMidH = (h + 1 == height) ? 0 : h + 1,
				bottomRightW = (w + 1 == width) ? 0 : w + 1,
				bottomRightH = (h + 1 == height) ? 0 : h + 1;

			pixels[i] = finalPixel;
			pixels[rightH * width + rightW].SetClamp(pixels[rightH * width + rightW] + Error * ALPHA); //right
			pixels[bottomLeftH * width + bottomLeftW].SetClamp(pixels[bottomLeftH * width + bottomLeftW] + Error * BETA); //right
			pixels[bottomMidH * width + bottomMidW].SetClamp(pixels[bottomMidH * width + bottomMidW] + Error * GAMMA); //right
			pixels[bottomRightH * width + bottomRightW].SetClamp(pixels[bottomRightH * width + bottomRightW] + Error * DELTA); //right
		}
	}
}

void ImageComposite(Image *bottom, Image *top, Image *result)
{
  // Extra Credit (Section 3.7).
  // This hook just takes the top image and bottom image, producing a result
  // You might want to define a series of compositing modes as OpenGL does
  // You will have to use the alpha channel here to create Mattes
  // One idea is to composite your face into a famous picture
}

void Image::Convolve(double **filter, double scale, int n, int absval) {
  // This is my definition of an auxiliary function for image convolution 
  // with an integer filter of width n and certain normalization.
  // The absval param is to consider absolute values for edge detection.
  
  // It is helpful if you write an auxiliary convolve function.
  // But this form is just for guidance and is completely optional.
  // Your solution NEED NOT fill in this function at all
  // Or it can use an alternate form or definition
	Image* newImg = new Image(width, height);
	int half = n / 2;
	double sigma = floor(n / 2.0) / 2.0;

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			int index = y * width + x;
			double	red = 0,
				green = 0,
				blue = 0,
				G = 0;

			//find final pixel value using filter
			ConvolvePixel(filter, scale, n, x, y, red, green, blue);

			if (absval > 0)
			{
				G = abs(Luminance((int)red, (int)green, (int)blue));
				newImg->pixels[index].SetClamp(G, G, G);
			}
			else
			{
				newImg->pixels[index].SetClamp(red, green, blue);
			}
		}
	}

	for (int i = 0; i < num_pixels; i++)
	{
		pixels[i] = newImg->pixels[i];
	}

	//memory leak
	delete newImg;
}

void Image::ConvolvePixel(double **filter, double scale, int n, const int x, const int y, double& red, double& green, double& blue)
{
	int half = n / 2;
	//find final pixel value using filter
	for (int i = 0; i < n; i++)
	{
		int finalY = y + i - half;

		//wrap edge
		//if (finalY < 0) finalY = (height - 1) + finalY;
		//if (finalY >= height) finalY = 0 + (finalY - (height - 1));

		//copy self value if pixel pass edg
		if (finalY < 0) finalY = 0;
		if (finalY >= height) finalY = y;

		for (int j = 0; j < n; j++)
		{
			int finalX = x + j - half;

			//wrap edge
			//if (finalX < 0) finalX = (width - 1) + finalX;
			//if (finalX >= width) finalX = 0 + (finalX - (width - 1));

			//copy edge value if pixel pass edg
			if (finalX < 0) finalX = 0;
			if (finalX >= width) finalX = x;

			double normalization = filter[i][j] / scale;

			red += normalization * (int)pixels[finalY * width + finalX].r;
			green += normalization * (int)pixels[finalY * width + finalX].g;
			blue += normalization * (int)pixels[finalY * width + finalX].b;
		}
	}
}

void Image::Blur(int n)
{
  /* Your Work Here (Section 3.4.1) */
	int half = n / 2;
	double sigma = floor(n / 2.0) / 2.0;
	double scale = 1;

	//filter initilization
	double** filter = new double*[n];
	for (int i = 0; i < n; i++)
	{
		filter[i] = new double[n]; 
	}
	
	//find the filter value
	gaussianFilter(sigma, filter, n, scale);

	//convolve
	Convolve(filter, scale, n, -1);

	//take care of memory leak
	for (int i = 0; i < n; i++)
	{
		delete[] filter[i];
	}

	delete[] filter;
}

void Image::Sharpen() 
{
  /* Your Work Here (Section 3.4.2) */
	double scale = 7;
	int n = 3, half = n / 2;

	//filter initilization
	double** filter = new double*[n];
	for (int i = 0; i < n; i++)
	{
		filter[i] = new double[n];
	}

	//initialize shapen filter
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			filter[i][j] = shapenFilter[i][j];
		}
	}

	Convolve(filter, scale, n, -1);

	//take care of memory leak
	for (int i = 0; i < n; i++)
	{
		delete[] filter[i];
	}

	delete[] filter;
}

void Image::EdgeDetect(int threshold)
{
  /* Your Work Here (Section 3.4.3) */
	//final pic
	Image* imgHoriz = new Image(*this);
	Image* imgVert = new Image(*this);

	//filter 
	int n = 3;
	int half = 1;
	double** filterHoriz = new double*[n];
	double** filterVert = new double*[n];
	for (int i = 0; i < n; i++)
	{
		filterHoriz[i] = new double[n];
		filterVert[i] = new double[n];
	}

	//initialize shapen filter
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			filterHoriz[i][j] = edgeDetectFilterH[i][j];
			filterVert[i][j] = edgeDetectFilterV[i][j];
		}
	}

	imgHoriz->Convolve(filterHoriz, 1, n, threshold);
	imgVert->Convolve(filterVert, 1, n, threshold);

	double G = 0 , Gx = 0, Gy = 0;
	for (int i = 0; i < num_pixels; i++)
	{
		Gx = imgHoriz->pixels[i].r;
		Gy = imgVert->pixels[i].r;
		G = sqrt((double)(abs(Gx) * abs(Gx) + abs(Gy) * abs(Gy)));

		G = (G > threshold) ? 255 : 0;
		pixels[i] = Pixel((int)G, (int)G, (int)G);
	}

	//take care of memory leak
	for (int i = 0; i < n; i++)
	{
		delete[] filterHoriz[i];
		delete[] filterVert[i];
	}

	delete[] filterHoriz;
	delete[] filterVert;
}

Image* Image::Scale(int sizex, int sizey)
{
  /* Your Work Here (Section 3.5.1) */
	double scaleX = (sizex * 1.0) / width;
	double scaleY = (sizey * 1.0) / height;
	Image* dst = new Image(sizex, sizey);
	int index = 0;

	//inverse Mapping
	for (int y = 0; y < sizey; y++)
	{
		for (int x = 0; x < sizex; x++)
		{
			double u = x / scaleX;
			double v = y / scaleY;
 			index = y * sizex + x;

			dst->pixels[index] = Sample(u, v, scaleX, scaleY);
		}
	}

	return dst;
}

void Image::Shift(double sx, double sy)
{
  /* Your Work Here (Section 3.5.2) */

	Image* dst = new Image(width, height);
	int index = 0;

	//inverse Mapping
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			double u = x - sx;
			double v = y - sy;
			index = y * width + x;

			dst->pixels[index] = Sample(u, v, 1, 1);
		}
	}

	for (int i = 0; i < num_pixels; i++)
	{
		pixels[i] = dst->pixels[i];
	}

	//memory leak
	delete dst;
}

/*
Image* Image::Rotate(double angle)
{
  // For rotation of the image
  // Not required in the assignment
  // But you can earn limited extra credit if you fill it in
  // (It isn't really that hard) 

    return NULL;
}
*/

void Image::Fun(int amplitude, int wavelength)
{
    /* Your Work Here (Section 3.6) */
	/* Your Work Here (Section 3.5.1) */
	Image* dst = new Image(width, height);
	int index = 0;
	double rotation = 0 * M_PI / 180.0;

	//Sampling Method
	SetSamplingMethod(1);

	//inverse Mapping
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			//ripple
			double u = x + amplitude * sin((y * 2 * M_PI) / (1.0 * wavelength));
			double v = y;

			//swirl
			//double xDist = u - width / 2.0;
			//double yDist = v - height / 2.0;
			//
			//double radian = sqrt(pow(xDist, 2.0) + pow(yDist, 2.0));
			//double cal = atan2(yDist, xDist) + rotation * (((height / 2.0) - radian) / (height / 2.0));

			//u = width / 2.0 + radian * cos(cal);
			//v = height / 2.0 + radian * sin(cal);

			if (u > width - 1) u = width - 1;
			if (u < 0) u = 0;
			if (v > height - 1) v = height - 1;
			if (v < 0) v = 0;

			index = y * width + x;

			dst->pixels[index] = pixels[(int)v * width + (int)u];
		}
	}

	for (int i = 0; i < num_pixels; i++)
	{
		pixels[i] = dst->pixels[i];
	}

	//memory leak
	delete dst;
}


Image* ImageMorph (Image* I0, Image* I1, int numLines, Line* L0, Line* L1, double t)
{
  /* Your Work Here (Section 3.7) */
  // This is extra credit.
  // You can modify the function definition. 
  // This definition takes two images I0 and I1, the number of lines for 
  // morphing, and a definition of corresponding line segments L0 and L1
  // t is a parameter ranging from 0 to 1.
  // For full credit, you must write a user interface to join corresponding 
  // lines.
  // As well as prepare movies 
  // An interactive slider to look at various morph positions would be good.
  // From Beier-Neely's SIGGRAPH 92 paper

    return NULL;
}


/**
 * Image Sample
 **/
void Image::SetSamplingMethod(int method)
{
  // Sets the filter to use for Scale and Shift
  // You need to implement point sampling, hat filter and mitchell

    assert((method >= 0) && (method < IMAGE_N_SAMPLING_METHODS));
    sampling_method = method;
}

Pixel Image::Sample (double u, double v, double sx, double sy)
{
  // To sample the image in scale and shift
  // This is an auxiliary function that it is not essential you fill in or 
  // you may define it differently.
  // u and v are the floating point coords of the points to be sampled.
  // sx and sy correspond to the scale values. 
  // In the assignment, it says implement MinifyX MinifyY MagnifyX MagnifyY
  // separately.  That may be a better way to do it.
  // This hook is primarily to get you thinking about that you have to have 
  // some equivalent of this function.

  if (sampling_method == IMAGE_SAMPLING_POINT) {
    // Your work here
	  int iu = (int)floor(u);
	  int iv = (int)floor(v);

	  return (iu < 0 || iu > width - 1 || iv < 0 || iv > height - 1) ? Pixel() : pixels[iv * width + iu];
  }

  else if (sampling_method == IMAGE_SAMPLING_HAT) {
	  // Your work here
	  int iu = (int)floor(u);
	  int iv = (int)floor(v);

	  if (iu < 0 || iu > width - 1 || iv < 0 || iv > height - 1)
	  {
		  return Pixel();
	  }

	  //find filter
	  int half = (int)std::max(std::max(1.0 / sx, 1.0 / sy), 1.0);
	  int n = 2 * half + 1;
	  double *fx = new double[n];
	  double *fy = new double[n];

	  double filterX = std::max(1.0 / sx, 1.0);
	  double filterY = std::max(1.0 / sy, 1.0);

	  for (int i = 0; i < n; i++)
	  {
		  int x = iu + (i - half);
		  int y = iv + (i - half);

		  fx[i] = abs(u - x) / filterX <= 1 ? 1 - abs(u - x) / filterX : 0;
		  fy[i] = abs(v - y) / filterY <= 1 ? 1 - abs(v - y) / filterY : 0;
	  }

	  //filter initilization
	  double normalization = 0;
	  double** filter = new double*[n];
	  for (int i = 0; i < n; i++)
	  {
		  filter[i] = new double[n];
	  }

	  for (int i = 0; i < n; i++)
	  {
		  for (int j = 0; j < n; j++)
		  {
			  filter[i][j] = fy[i] * fx[j];
			  normalization += filter[i][j];
		  }
	  }

	  //convolve
	  Pixel finalPixel;
	  double red = 0, green = 0, blue = 0;

	  ConvolvePixel(filter, normalization, n, iu, iv, red, green, blue);

	  finalPixel.SetClamp((int)red, (int)green, (int)blue);

	  //take care of memory leak
	  for (int i = 0; i < n; i++)
	  {
		  delete[] filter[i];
	  }

	  delete[] filter;
	  delete[] fx;
	  delete[] fy;

	  return finalPixel;
  }

  else if (sampling_method == IMAGE_SAMPLING_MITCHELL) {
	  // Your work here
	  int iu = (int)floor(u);
	  int iv = (int)floor(v);

	  if (iu < 0 || iu > width - 1 || iv < 0 || iv > height - 1)
	  {
		  return Pixel();
	  }

	  //find filter
	  int half = (int)std::max(std::max(1.0 / sx, 1.0 / sy), 2.0);
	  int n = 2 * half + 1;
	  double *fx = new double[n];
	  double *fy = new double[n];

	  double filterX = std::max(1.0 / sx, 1.0);
	  double filterY = std::max(1.0 / sy, 1.0);

	  for (int i = 0; i < n; i++)
	  {
		  int x = iu + (i - half);
		  int y = iv + (i - half);
		  double absX = abs(u - x) / filterX;
		  double absY = abs(v - y) / filterY;

		  if (0 <= absX && absX < 1)
		  {
			  fx[i] = (7 * pow(absX, 3.0) - 12 * pow(absX, 2.0) + 16.0 / 3.0) / 6.0;
		  }
		  else if (1 <= absX && absX < 2)
		  {
			  fx[i] = (-7.0 / 3.0 * pow(absX, 3.0) + 12 * pow(absX, 2.0) - 20 * absX + 32.0 / 3.0) / 6.0;
		  }
		  else
		  {
			  fx[i] = 0;
		  }

		  if (0 <= absY && absY < 1)
		  {
			  fy[i] = (7 * pow(absY, 3.0) - 12 * pow(absY, 2.0) + 16.0 / 3.0) / 6.0;
		  }
		  else if (1 <= absY && absY < 2)
		  {
			  fy[i] = (-7.0 / 3.0 * pow(absY, 3.0) + 12 * pow(absY, 2.0) - 20 * absY + 32.0 / 3.0) / 6.0;
		  }
		  else
		  {
			  fy[i] = 0;
		  }
	  }

	  //filter initilization
	  double normalization = 0;
	  double** filter = new double*[n];
	  for (int i = 0; i < n; i++)
	  {
		  filter[i] = new double[n];
	  }

	  for (int i = 0; i < n; i++)
	  {
		  for (int j = 0; j < n; j++)
		  {
			  filter[i][j] = fy[i] * fx[j];
			  normalization += filter[i][j];
		  }
	  }

	  //convolve
	  Pixel finalPixel;
	  double red = 0, green = 0, blue = 0;

	  ConvolvePixel(filter, normalization, n, iu, iv, red, green, blue);

	  finalPixel.SetClamp((int)red, (int)green, (int)blue);

	  //take care of memory leak
	  for (int i = 0; i < n; i++)
	  {
		  delete[] filter[i];
	  }

	  delete[] filter;
	  delete[] fx;
	  delete[] fy;

	  return finalPixel;
  }

  else {
    fprintf(stderr,"I don't understand what sampling method is used\n") ;
    exit(1) ;
  }

  return Pixel() ;
}

void SetPatch(Image* img, int fromPatchWidth, int fromPatchHeight, Pixel** allInputImgPatches, int pIndex, int size, int overlapSize)
{
	int imgSize = img->height * img->width;
	for (int i = 0; i < size; i++)
	{
		int imgHeightIndex = fromPatchHeight * img->width * (size - overlapSize) + i * img->width;
		if (imgHeightIndex >= imgSize)
		{
			break;
		}

		for (int j = 0; j < size; j++)
		{
			int imgWidthIndex = fromPatchWidth * (size - overlapSize) + j;
			if (imgWidthIndex >= img->width)
			{
				break;
			}

			img->pixels[imgHeightIndex + imgWidthIndex] = allInputImgPatches[pIndex][i * size + j];
		}
	}
}

void findMinCut(Image* img, int fromPatchWidth, int fromPatchHeight, Pixel** allInputImgPatches, int size, int overlapSize, int** newImagePatchIndex, int** path, bool isLeft)
{
	int imgSize = img->height * img->width;
		float ** errorList = new float*[size];
		float ** minCut = new float*[size + 1];

	int endHeight = size;

		for (int i = 0; i < size + 1; i++)
		{
			if (i < size)
			{
				path[i] = new int[overlapSize] {};
				errorList[i] = new float[overlapSize] {};
			}
			minCut[i] = new float[overlapSize + 2] {};
		}

		for (int i = 0; i < size + 1; i++)
		{
			if (i < overlapSize + 1)
			{
				minCut[0][i] = 0.0f;
			}

		minCut[i][0] = (float)INT_MAX;
		minCut[i][overlapSize + 1] = (float)INT_MAX;
		}

	int imgHeightIndex = 0;
	int imgWidthIndex = 0;

		for (int i = 0; i < size; i++)
		{
		if (isLeft)
		{
			imgHeightIndex = fromPatchHeight * img->width * (size - overlapSize) + i * img->width;
			if (imgHeightIndex >= imgSize)
			{
				endHeight = i;
				break;
			}
		}
		else
		{
			imgWidthIndex = fromPatchWidth * (size - overlapSize) + i;
			if (imgWidthIndex >= img->width)
			{
				endHeight = i;
				break;
			}
		}

			for (int j = 0; j < overlapSize; j++)
			{
			if (isLeft)
			{
				imgWidthIndex = fromPatchWidth * (size - overlapSize) + j;
				if (imgWidthIndex >= img->width)
				{
					break;
				}
			}
			else
			{
				imgHeightIndex = fromPatchHeight * img->width * (size - overlapSize) + j * img->width;
				if (imgHeightIndex >= imgSize)
				{
					break;
				}
			}


				int leftIndex = imgHeightIndex + imgWidthIndex;
				int pixelIndex = i * size + j;

				float err = (float)pow(img->pixels[leftIndex].Luminance() - allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][pixelIndex].Luminance(), 2.0f);
				errorList[i][j] = err;
				minCut[i + 1][j + 1] = err + std::min(std::min(minCut[i][j + 1], minCut[i][j]), minCut[i][j + 2]);
			}
		}

		//for (int i = 0; i <= size; i++)
		//{
		//	for (int j = 0; j <= overlapSize + 1; j++)
		//	{
		//		fprintf(stderr, "%.2f\t", minCut[i][j]);
		//	}
		//	fprintf(stderr, "\n");
		//}

		//find min error path
		int minIndex;
		float minErr = (float)INT_MAX;
	int lastHeight = endHeight;
		for (int i = 1; i <= overlapSize; i++)
		{
		if (minCut[lastHeight][i] < minErr)
			{
			minErr = minCut[lastHeight][i];
				minIndex = i;
			}
		}

	path[lastHeight - 1][minIndex - 1] = 1;
	for (int i = lastHeight - 1; i > 0; i--)
		{
			minErr -= errorList[i][minIndex - 1];

			for (int j = 1; j <= overlapSize; j++)
			{
				if (minCut[i][j] == minErr)
				{
					path[i - 1][j - 1] = 1;
					minIndex = j;
				}
			}
		}

		//for (int i = 0; i < size; i++)
		//{
		//	for (int j = 0; j < overlapSize; j++)
		//	{
		//		fprintf(stderr, "%d\t", path[i][j]);
		//	}
		//	fprintf(stderr, "\n");
		//}

		for (int i = 0; i < size + 1; i++)
		{
			if (i < size)
			{
				delete[] errorList[i];
			}

			delete[] minCut[i];
		}

		delete[] errorList;
		delete[] minCut;
	}

void SetQuiltingPatch(Image* img, int fromPatchWidth, int fromPatchHeight, Pixel** allInputImgPatches, int size, int overlapSize, int** newImagePatchIndex)
	{
	int imgSize = img->height * img->width;
	int fromIIndex = fromPatchHeight == 0 ? 0 : overlapSize;
	int fromJIndex = fromPatchWidth == 0 ? 0 : overlapSize;

	int ** pathL = NULL;
	int ** pathT = NULL;

	if (fromJIndex != 0)
		{
		pathL = new int*[size];
		findMinCut(img, fromPatchWidth, fromPatchHeight, allInputImgPatches, size, overlapSize, newImagePatchIndex, pathL, true);
	}

	if (fromIIndex != 0)
			{
		pathT = new int*[size];
		findMinCut(img, fromPatchWidth, fromPatchHeight, allInputImgPatches, size, overlapSize, newImagePatchIndex, pathT, false);
		}

	int iIntersect = 0;
	bool foundIntersect = false;
	if (pathT && pathL)
	{
		for (int i = 0; i < overlapSize; i++)
		{
			for (int j = 0; j < overlapSize; j++)
		{
				if (pathT[i][j] == 1 && pathL[i][j] == 1)
			{
					foundIntersect = true;
					iIntersect = i;
					break;
				}
			}

			if (foundIntersect) break;
		}
		}

	if (pathL)
	{
		for (int i = iIntersect; i < size; i++)
		{
			bool boundary = false;
			int imgHeightIndex = fromPatchHeight * img->width * (size - overlapSize) + i * img->width;
			if (imgHeightIndex >= imgSize)
			{
				break;
		}

			for (int j = 0; j < overlapSize; j++)
		{
				int imgWidthIndex = fromPatchWidth * (size - overlapSize) + j;
				if (imgWidthIndex >= img->width)
			{
					break;
		}

				if (boundary)
		{
					img->pixels[imgHeightIndex + imgWidthIndex] = allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][i * size + j];
				}
				else
			{
					if (pathL[i][j] == 1)
				{
						boundary = true;
						int r = (img->pixels[imgHeightIndex + imgWidthIndex].r + allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][i * size + j].r) / 2;
						int g = (img->pixels[imgHeightIndex + imgWidthIndex].g + allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][i * size + j].g) / 2;
						int b = (img->pixels[imgHeightIndex + imgWidthIndex].b + allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][i * size + j].b) / 2;
						img->pixels[imgHeightIndex + imgWidthIndex] = Pixel(r, g, b);
						//img->pixels[imgHeightIndex + imgWidthIndex] = Pixel(255, 0, 0);
					}
				}
			}
		}

		for (int i = 0; i < size; i++)
		{
			delete[] pathL[i];
		}

		delete[] pathL;
	}

	if (pathT)
	{
		for (int i = iIntersect; i < size; i++)
		{
			bool boundary = false;
			int imgWidthIndex = fromPatchWidth * (size - overlapSize) + i;
			if (imgWidthIndex >= img->width)
			{
				break;
			}

			for (int j = 0; j < overlapSize; j++)
			{
				int imgHeightIndex = fromPatchHeight * img->width * (size - overlapSize) + j * img->width;
				if (imgHeightIndex >= imgSize)
				{
					break;
				}

				if (boundary)
				{
					img->pixels[imgHeightIndex + imgWidthIndex] = allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][j * size + i];
				}
				else
				{
					if (pathT[i][j] == 1)
					{
						boundary = true;
						int r = (img->pixels[imgHeightIndex + imgWidthIndex].r + allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][j * size + i].r) / 2;
						int g = (img->pixels[imgHeightIndex + imgWidthIndex].g + allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][j * size + i].g) / 2;
						int b = (img->pixels[imgHeightIndex + imgWidthIndex].b + allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][j * size + i].b) / 2;
						img->pixels[imgHeightIndex + imgWidthIndex] = Pixel(r, g, b);
						img->pixels[imgHeightIndex + imgWidthIndex] = Pixel(255, 0, 0);
					}
				}
			}
		}

		for (int i = 0; i < size; i++)
			{
			delete[] pathT[i];
		}

		delete[] pathT;
	}

	for (int i = fromIIndex; i < size; i++)
	{
		int imgHeightIndex = fromPatchHeight * img->width * (size - overlapSize) + i * img->width;
		if (imgHeightIndex >= imgSize)
		{
			break;
		}

		for (int j = fromJIndex; j < size; j++)
		{
			int imgWidthIndex = fromPatchWidth * (size - overlapSize) + j;
			if (imgWidthIndex >= img->width)
			{
				break;
			}

			img->pixels[imgHeightIndex + imgWidthIndex] = allInputImgPatches[newImagePatchIndex[fromPatchHeight][fromPatchWidth]][i * size + j];
		}
	}
}

float ssdL(Pixel* left, Pixel* p, int overlapSize, int size)
{
	float err = 0.0f;
	if (left)
	{
		int from = size - overlapSize;
		for (int i = 0; i < size; i++)
		{
			for (int j = from; j < size; j++)
			{
				int leftIndex = i * size + j;
				int pIndex = i * size + (j - from);

				err += (float)pow(left[leftIndex].Luminance() - p[pIndex].Luminance(), 2.0f);
				//err += (float)pow(left[leftIndex].r - p[pIndex].r, 2.0f);
				//err += (float)pow(left[leftIndex].g - p[pIndex].g, 2.0f);
				//err += (float)pow(left[leftIndex].b - p[pIndex].b, 2.0f);
			}
		}
	}

	return err;
}

float ssdT(Pixel* top, Pixel* p, int overlapSize, int size)
{
	float err = 0.0f;
	if (top)
	{
		int from = size - overlapSize;
		for (int i = from; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				int topIndex = i * size + j;
				int pIndex = (i - from) * size + j;

				err += (float)pow(top[topIndex].Luminance() - p[pIndex].Luminance(), 2.0f);
				//err += (float)pow(top[topIndex].r - p[pIndex].r, 2.0f);
				//err += (float)pow(top[topIndex].g - p[pIndex].g, 2.0f);
				//err += (float)pow(top[topIndex].b - p[pIndex].b, 2.0f);
			}
		}
	}

	return err;
}

typedef std::pair<float, int> ErrorPair;

int bestMatch(int i, int j, int** newImagePatchIndex, Pixel** allInputImgPatches, int patchesSize, int overlapSize, int size)
{
	Pixel* left = NULL;
	Pixel* top = NULL;
	std::vector<ErrorPair> errorList;
	float tolerance = 1.1f;

	if (i == 0)
	{
		left = allInputImgPatches[newImagePatchIndex[i][j - 1]];
	}
	else if (j == 0)
	{
		top = allInputImgPatches[newImagePatchIndex[i - 1][j]];
	}
	else
	{
		left = allInputImgPatches[newImagePatchIndex[i][j - 1]];
		top = allInputImgPatches[newImagePatchIndex[i - 1][j]];
	}

	for (int k = 0; k < patchesSize; k++)
	{
		float err = ssdT(top, allInputImgPatches[k], overlapSize, size) + ssdL(left, allInputImgPatches[k], overlapSize, size);
		errorList.push_back(ErrorPair(err, k));
	}

	float errorTolerance = std::min_element(errorList.begin(), errorList.end(), [](ErrorPair e1, ErrorPair e2){
		return e1.first < e2.first && e1.first != 0.0f;
	})->first * tolerance;

	std::vector<int> validPatches;
	for (int i = 0; i < (int)errorList.size(); i++)
	{
		if (errorList[i].first <= errorTolerance)
		{
			validPatches.push_back(errorList[i].second);
		}
	}

	return validPatches[rand() % validPatches.size()];
}

Image* Image::ImageQuilting(int x, int y, int size)
{
	srand(time(NULL));
	Image* dst = new Image(x, y);

	Pixel** allInputImgPatches;
	int overlapSize = std::max(1, size / 6);
	int patchesWidth = width - size + 1;
	int patchesHeight = height - size + 1;
	int patchesSize = patchesWidth * patchesHeight;

	allInputImgPatches = new Pixel*[patchesSize];
	for (int i = 0; i < patchesSize; i++)
	{
		allInputImgPatches[i] = new Pixel[size * size];
	}

	for (int pHeight = 0; pHeight < patchesHeight; pHeight++)
	{
		for (int pWidth = 0; pWidth < patchesWidth; pWidth++)
		{
			for (int pArrHeight = 0; pArrHeight < size; pArrHeight++)
			{
				for (int pArrWidth = 0; pArrWidth < size; pArrWidth++)
				{
					int patchIndex = pHeight * patchesWidth + pWidth;
					int pixelIndex = pHeight * width + pWidth;
					int pArrIndex = pArrHeight * size + pArrWidth;
					allInputImgPatches[patchIndex][pArrIndex] = pixels[pixelIndex + pArrHeight * width + pArrWidth];
				}
			}
		}
	}

	int maxNewImgPatchWidth = (int)ceil((float)x / (float)(size - overlapSize));
	int maxNewImgPatchHeight = (int)ceil((float)y / (float)(size - overlapSize));

	int** newImagePatchIndex = new int*[maxNewImgPatchHeight]; //store all patch index used for new image
	for (int i = 0; i < maxNewImgPatchWidth; i++)
	{
		newImagePatchIndex[i] = new int[maxNewImgPatchWidth];
	}

	for (int i = 0; i < maxNewImgPatchHeight; i++)
	{
		for (int j = 0; j < maxNewImgPatchWidth; j++)
		{
			if (i == 0 && j == 0)
			{
				int randomPatchIndex = rand() % patchesSize;
				SetPatch(dst, j, i, allInputImgPatches, randomPatchIndex, size, overlapSize);
				newImagePatchIndex[i][j] = randomPatchIndex;
			}
			else
			{
				int bestMatchIndex = bestMatch(i, j, newImagePatchIndex, allInputImgPatches, patchesSize, overlapSize, size);
				newImagePatchIndex[i][j] = bestMatchIndex;

				//SetPatch(dst, j, i, allInputImgPatches, bestMatchIndex, size, overlapSize);
				SetQuiltingPatch(dst, j, i, allInputImgPatches, size, overlapSize, newImagePatchIndex);
			}

			fprintf(stderr, "Progress: %.2f%%\r", (float)(i * maxNewImgPatchWidth + j) / (float)(maxNewImgPatchHeight * maxNewImgPatchWidth) * 100.0f);
		}
	}

	for (int i = 0; i < patchesSize; i++)
	{
		delete[] allInputImgPatches[i];
	}

	delete[] allInputImgPatches;
	return dst;
}

Image* Image::ImageTiling(int x, int y, int size)
{
	Image* dst = new Image(x, y);

	Pixel** allInputImgPatches;
	int patchesWidth = width - size + 1;
	int patchesHeight = height - size + 1;
	int patchesSize = patchesWidth * patchesHeight;

	allInputImgPatches = new Pixel*[patchesSize];
	for (int i = 0; i < patchesSize; i++)
	{
		allInputImgPatches[i] = new Pixel[size * size];
	}

	for (int pHeight = 0; pHeight < patchesHeight; pHeight++)
	{
		for (int pWidth = 0; pWidth < patchesWidth; pWidth++)
		{
			for (int pArrHeight = 0; pArrHeight < size; pArrHeight++)
			{
				for (int pArrWidth = 0; pArrWidth < size; pArrWidth++)
				{
					int patchIndex = pHeight * patchesWidth + pWidth;
					int pixelIndex = pHeight * width + pWidth;
					int pArrIndex = pArrHeight * size + pArrWidth;
					allInputImgPatches[patchIndex][pArrIndex] = pixels[pixelIndex + pArrHeight * width + pArrWidth];
				}
			}
		}
	}

	int maxNewImgPatchWidth = (int)ceil((float)x / (float)size);
	int maxNewImgPatchHeight = (int)ceil((float)y / (float)size);
	for (int i = 0; i < maxNewImgPatchHeight; i++)
	{
		for (int j = 0; j < maxNewImgPatchWidth; j++)
		{
			int randomPatchIndex = rand() % patchesSize;
			SetPatch(dst, j, i, allInputImgPatches, randomPatchIndex, size, 0);
		}
	}

	for (int i = 0; i < patchesSize; i++)
	{
		delete[] allInputImgPatches[i];
	}

	delete[] allInputImgPatches;
	return dst;
	return NULL;
}


