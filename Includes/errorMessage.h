#include <string>

#ifndef ERROR_MSG_H
#define ERROR_MSG_H

using namespace std;

class ErrorMSG {
public:
	const static string inputIsNegative;
	const static string inputOutOfRange;
	const static string blurInputInvalid;
	const static string inputIsNotDivisible;
};

#endif