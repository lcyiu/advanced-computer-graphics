////////////////////////////////////////
// display.h
////////////////////////////////////////

#ifndef CSE169_DISPLAY_H
#define CSE169_DISPLAY_H

#include "camera.h"
#include <glui.h>

#define OPENID 0
#define SAVEID 1

#define OPENPANELID 2
#define CANCELID 6
#define ADDID 7

#define DELETEONEID 3
#define CLEARALLID 4
#define FILEBROWSERID 5

#define IMAGETILINGID 8
#define IMAGEQUILTINGID 9

////////////////////////////////////////////////////////////////////////////////

class Display {
public:
	Display();
	~Display();

	void Update();
	void Reset();
	void Draw();

	void Quit();

	// Event handlers
	void Resize(int x, int y);
	void Keyboard(int key, int x, int y);
	void MouseButton(int btn, int state, int x, int y);
	void MouseMotion(int x, int y);

private:
	// Window management
	int WinX, WinY;

	// Input
	bool LeftDown, MiddleDown, RightDown;
	int MouseX, MouseY;

	// Components
	Camera Cam;
};

////////////////////////////////////////////////////////////////////////////////

#endif
